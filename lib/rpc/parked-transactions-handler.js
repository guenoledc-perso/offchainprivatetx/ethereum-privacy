const EthUtils = require('ethereumjs-util');
const Transaction = require('ethereumjs-tx');
const BN = EthUtils.BN;
const RpcUtil = require('ethereum-standalone-node/lib/rpc/rpc-util');
const RPCError = RpcUtil.RPCError;
const assertParams = RpcUtil.assertParams;



class ParkedTransactions {
    static getFromPrivacyNode(privacyNode) {
        if(!privacyNode) throw new TypeError('expecting a valid privacyNode to get its ParkedTransaction')
        if(!privacyNode.parkedTransactions) return new ParkedTransactions(privacyNode)
        else return privacyNode.parkedTransactions
    }
    constructor(privacyNode) {
        if(!privacyNode) throw new TypeError('expecting a valid privacyNode to create a ParkedTransaction class')
        this.privacyNode = privacyNode
        this.privacyNode.parkedTransactions = this;

        this.parkedRequests = {} // map on the tx hash
    }

    parkRawTransaction(request) {
        if(!request || !request.method || request.method!='eth_sendRawTransaction' || !Array.isArray(request.params) || request.params.length!=1) throw new TypeError('expecting a sendRawTransaction request')
        let tx = new Transaction(request.params[0])
        let hash = '0x'+tx.hash().toString('hex')
        this.parkedRequests[hash]=request
        return hash
    }
    getRawTransaction(hash) {
        if(hash in this.parkedRequests) return this.parkedRequests[hash]
        else return null
    }
    listParked() {
        return Object.keys(this.parkedRequests)
    }
    removeRawTransaction(hash) {
        delete this.parkedRequests[hash]
    }
}

function handle_parked(req, callback) {
    let self=this // this is the privacyNode
    let parked = ParkedTransactions.getFromPrivacyNode(self)
    const functions = {
        'rawTransactions': (req, cb) => {
            cb(null, parked.listParked())
        },
        'sendTransaction': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'hash', default:undefined, format:'Data32'},
                {name:'private', default:[], format:'Object'}
            ]);
            const handle_forwarding = require('./forwarding-rpc-handler')
            try {
                let request= parked.getRawTransaction(params.hash)
                if(request) {
                    parked.removeRawTransaction(params.hash)
                    request.params.push(params.private)
                    handle_forwarding.apply(self, [request, cb])
                } else cb('Hash not found in parked transactions')

            } catch(error) {
                cb(new RPCError('Fail getting transaction by hash', error, RpcUtil.INVALID_PARAMS))
            }
        },
    }
    let method = req.method.split('_')[1];
    
    if(functions[method]) functions[method].apply(this, [req, callback]);
    else callback(new RPCError('unknown parked method '+req.method, null, RpcUtil.METHOD_NOT_FOUND), null);    
}


module.exports = {
    ParkedTransactions: ParkedTransactions,
    handle_parked:handle_parked,
}