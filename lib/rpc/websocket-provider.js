const WebSocketProviderOrigin = require('web3-providers-ws')

class Filters {
    static isNewFilterMethod(payload) {
        return (payload && payload.method && 
            ['eth_newFilter', 'eth_newBlockFilter', 'eth_newPendingTransactionFilter'].includes(payload.method) )
    }
    static isFilterActionMethod(payload) {
        return (payload && payload.method && 
            ['eth_uninstallFilter', 'eth_getFilterChanges', 'eth_getFilterLogs'].includes(payload.method) )
    }
    static isFilterUninstall(payload) {
        return (payload && payload.method && 
            ['eth_uninstallFilter'].includes(payload.method) )
    }    
    static isRecreateFilter(payload) {
        return (payload && payload.method && 
            ['special_recreateFilter'].includes(payload.method) )
    }
    constructor(wsProvider) {
        this.wsProvider = wsProvider
        this.filters = {} // map client id to a {method:'eth_newXxx', params:[], clientId, sourceId}
        this.pendings = {} // map by jsonrpc id to a clientId
    }

    handleIncoming(payloads) {
        if(!Array.isArray(payloads)) return this.handleIncoming([payloads])
        let self = this
        payloads.forEach(load => {
            if(Filters.isNewFilterMethod(load)) self.pendings[load.id] = {method: load.method, params: load.params}
            if(Filters.isFilterActionMethod(load) && load.params[0] && load.params[0] in self.filters) {
                const clientId = load.params[0]
                self.pendings[load.id]=clientId
                load.params[0] = self.filters[clientId].sourceId // remap to the id know by the provider
                if(Filters.isFilterUninstall(load)) delete self.filters[clientId] // we won't need it anymore
            }
            if(Filters.isRecreateFilter(load)) {
                const clientId = load.params[0]
                self.pendings[load.id] = { clientId:clientId }
                load.method = self.filters[clientId].method
                load.params = self.filters[clientId].params // ATTENTION: If fromBlock is in the past, it may trigger historical data again, let to what ??
            }
        })
    }

    handleOutgoing(responses) {
        if(!Array.isArray(responses)) return this.handleOutgoing([responses])
        let self = this
        responses.forEach(resp => {
            if(!resp || !resp.id || resp.error || !resp.result ) return
            if( !(resp.id in self.pendings) ) return
            const pending = self.pendings[resp.id] // either the params for the creation or the clientId
            delete self.pendings[resp.id]
            if(pending.method && pending.params) // create a new Filter
                self.filters[resp.result] =  Object.assign(pending, {clientId:resp.result, sourceId:resp.result})
            else if(pending.clientId) // recreating a filter so set the new sourceId
                self.filters[pending.clientId].sourceId = resp.result
        })
    }

    onConnectionReset() {
        // here we assume that all previously recorded filters are lost from the providers and we must regenerate them
        let count=1
        let self = this
        Object.values(this.filters).forEach( filter=>{
            self.wsProvider.sendAsync({jsonrpc:"2.0", id:count++, method:'special_recreateFilter', params:[filter.clientId]}, (err, response)=>{
                if(err) delete this.filters[filter.clientId] // remove if we cannot recreate it
            })
        })
    }


}

class WebSocketProvider 
{ 
    constructor(url, options) {
        this.url = url
        this.options = options
        
        this._my_reqId=1
        this._trace = !!options.trace

        this.filters = new Filters(this)
        this._initConnection() 
    }
    _initConnection() {
        if(this._ws_provider) { this._ws_provider.disconnect(); this._ws_provider=null; }
        this._ws_provider = new WebSocketProviderOrigin(this.url, this.options)
        this.filters.onConnectionReset() // first time it will do nothing
    }
    _triggerConnectionReset(delay) {
        if(!delay) {
            if(this._timerTrigger) {
                clearTimeout(this._timerTrigger); 
                //console.log('Trigger off'); 
                this._timerTrigger=null
            }
            return // in all case do nothing more as there is no delay defined
        }
        let self = this
        if(!this._timerTrigger) {// no timer, create it
            //console.log('Trigger on for', delay)
            this._timerTrigger = setTimeout( ()=>{
                self._triggerConnectionReset(false) // reset the trigger
                self._initConnection()
                //console.log('WS Connection reset.')
            }, delay)
        }
    }
    _waitConnectionReady(recallUntilReady) {
        this._triggerConnectionReset(1000) // if connection cannot be established in 500 ms reset the connection
        if( !this._ws_provider.connected ) {
            setTimeout( recallUntilReady , 20 )
            //process.stdout.write('.')
            return true
        } else this._triggerConnectionReset(false) // reset the waiting
        return false
    }

    send(payload) { throw new Error('Synchronous call is not supported in web socket') }

    sendAsync(payload, callback) {
        // First things, try if the connection is ok and if not close and reinitiate a new one
        if( this._waitConnectionReady( this.sendAsync.bind(this, payload, callback) ) ) 
            return // if it returns true then this is bacause it needed to wait
    

        const isArray = Array.isArray(payload) // remember if the payload was in an array
        
        // force a continuous id because the original websocket uses a mapping with the id to ensure returning to the correct callback
        const payloads = isArray?payload:[payload] // put always to array format
        let mapIds = {} // to remember the id translation, then translate each item in the array
        const self = this

        self.filters.handleIncoming(payloads) // modify the payload if needed

        payloads.forEach(load=>{
            mapIds[this._my_reqId]=load.id; 
            load.id=this._my_reqId++
            if(self._trace) console.log('===>', isArray?'M':'S','id received:', mapIds[load.id] , JSON.stringify(load)); 
        })

        self._ws_provider.send(payload, (err, response)=>{
            if(err) return callback(err, null)
            if(!response) return callback('public node returned null response', null)
            // put response in array always
            let responses = Array.isArray(response)?response:[response]

            // map back to the original id because the web3 methods uses id mapping to find the callback in case of filters
            responses.forEach(resp=>{
                if(self._trace) console.log('<===', isArray?'M':'S','id received:', mapIds[resp.id], JSON.stringify(resp))
                resp.id=mapIds[resp.id]
            }) 
            // remap also the payloads because they are used in handling the response
            payloads.forEach(load=>{
                load.id=mapIds[load.id]
            })
    

            //console.log('<<<<<<', JSON.stringify(responses))
            self.filters.handleOutgoing(responses) // modify the response if needed

            // if not an array initially take the first item only
            if(!isArray ) response = responses[0]
            else response = responses

            //console.log('<|||||', JSON.stringify(response))
    
            callback(err, response)
        }) 
    } 
    isConnected(){ return true; } // for the moment !!
}

module.exports = WebSocketProvider