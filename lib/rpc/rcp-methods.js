const methods = {
    'web3_clientVersion':{useDefault:true},
    'web3_sha3':{useDefault:true},
    'net_version':{useDefault:true}, // Metamask needs to have a number to sign the transaction for the private node
    'net_listening':{useDefault:true},

    'eth_protocolVersion':{useDefault:true},
    'eth_syncing':{useDefault:true},
    'eth_coinbase':{useDefault:true},
    'eth_mining':{useDefault:true},
    'eth_hashrate':{useDefault:true},
    'eth_gasPrice':{useDefault:true},
    'eth_accounts':{useDefault:true},
    'eth_blockNumber':{useDefault:true},
    'eth_sign':{useWallet:true, address:req=>req.params[0]},
    

    'eth_getBlockTransactionCountByHash':{useBlockHash:true, hash:req=>req.params[0]},
    'eth_getBlockTransactionCountByNumber':{useDefault:true},
    'eth_getUncleCountByBlockHash':{useBlockHash:true, hash:req=>req.params[0]},
    'eth_getUncleCountByBlockNumber':{useDefault:true},
    'eth_getBlockByHash':{useBlockHash:true, hash:req=>req.params[0]},
    'eth_getBlockByNumber':{useDefault:true},
    'eth_getTransactionByBlockHashAndIndex':{useBlockHash:true, hash:req=>req.params[0]},
    'eth_getTransactionByBlockNumberAndIndex':{useDefault:true},
    'eth_getUncleByBlockHashAndIndex':{useBlockHash:true, hash:req=>req.params[0]},
    'eth_getUncleByBlockNumberAndIndex':{useDefault:true},

    'eth_getBalance':{useDefault:true},
    'eth_getStorageAt':{useDefault:true},
    'eth_getTransactionCount':{useDefault:true},
    'eth_getCode':{useDefault:true},
    'eth_call':{useDefault:true},
    'eth_estimateGas':{useDefault:true},

    'eth_sendTransaction':{needEncryption:req=>req.params[1] && Array.isArray(req.params[1].privateFor), for:req=>req.params[1].privateFor, update:req=>req.params.pop()},
    'eth_sendRawTransaction':{needEncryption:req=>req.params[1] && Array.isArray(req.params[1].privateFor), for:req=>req.params[1].privateFor, update:req=>req.params.pop()},

    'eth_getTransactionByHash':{useTxHash:true, hash:req=>req.params[0]},
    'eth_getTransactionReceipt':{useTxHash:true, hash:req=>req.params[0]},

    'eth_newFilter':{useDefault:true, onResponse:{multiplexFilter:true}}, // register the filter on both nodes ! needs multiplexing 
    'eth_newBlockFilter':{useDefault:true, onResponse:{multiplexFilter:true}},
    'eth_newPendingTransactionFilter':{useDefault:true, onResponse:{multiplexFilter:true}},
    'eth_uninstallFilter':{useFilterId:true, id:req=>req.params[0]},
    'eth_getFilterChanges':{useFilterId:true, id:req=>req.params[0], onResponse:{setLogOrigin:true}},
    'eth_getFilterLogs':{useFilterId:true, id:req=>req.params[0], onResponse:{setLogOrigin:true}},
    'eth_getLogs':{useDefault:true, onResponse:{setLogOrigin:true}},

    'personal_unlockAccount':{useWallet:true, address:req=>req.params[0]},
    'personal_ecRecover':{useDefault:true},
    'personal_sign':{useWallet:true, address:req=>req.params[1]},
    'personal_sendTransaction':{useWallet:true, address:req=>req.params[1]},
    'personal_newAccount':{useDefault:true},
    'personal_lockAccount':{useWallet:true, address:req=>req.params[0]},
    'personal_listAccounts':{useDefault:true},
    'personal_importRawKey':{useDefault:true},
}

module.exports = methods