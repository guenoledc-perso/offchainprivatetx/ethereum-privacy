const async = require('async');
const RPCHandlerApp = require('ethereum-standalone-node/lib/rpc/app-rpc-handler')
const RpcUtil = require('ethereum-standalone-node/lib/rpc/rpc-util')
const METHOD_NOT_FOUND=RpcUtil.METHOD_NOT_FOUND;

const RPCError = RpcUtil.RPCError;
const RPCResponse = RpcUtil.RPCResponse;

const handlerMap = {
    'web3': require('./forwarding-rpc-handler'),
    'net': require('./forwarding-rpc-handler'),
    'eth': require('./forwarding-rpc-handler'),
    'personal': require('./forwarding-rpc-handler'),
    'miner': require('./forwarding-rpc-handler'),
    'parked': require('./parked-transactions-handler').handle_parked,
}

class PrivacyRPCHandlerApp extends RPCHandlerApp {
    constructor(privacyNode, trace) {
        if(!privacyNode ) throw new TypeError('Expects a valid EthereumPrivacyNode')
        super(privacyNode, handlerMap, trace) // this replaces the esn by privacyNode being this in the handlers
        this.privacyNode = privacyNode
        this.handlers.admin = this.adminHandler.bind(this)
    }

    answerModuleList(req, callback) {
        if(req.method=='rpc_modules') {
            callback(null, {  
                "eth":"1.0",
                "net":"1.0",
                "personal":"1.0",
                "web3":"1.0",
                "admin":"1.0",
                "parked":"1.0"
             })
        } else callback(new RPCError('unknown method '+req.method, null, METHOD_NOT_FOUND), null);
    }
    adminHandler(req, callback) {
        let params

        if(req.method=='admin_setDefaultNode') {
            params = RpcUtil.assertParams(req.params, [
                {name: 'defaultNode', format:'UTF8'}
            ])
            params.defaultNode = params.defaultNode.toString('utf8')
            if(['private', 'public'].includes(params.defaultNode)) {
                this.privacyNode.defaultNode = params.defaultNode
                callback(null, this.privacyNode.defaultNode) 
            } else {
                callback('Invalid default node: '+params.defaultNode, null)
            }


        } else if(req.method=='admin_getDefaultNode') {
            callback(null, this.privacyNode.defaultNode)


        } else if(req.method=='admin_getPublicKey') {
            params = RpcUtil.assertParams(req.params, [
                {name: 'format', default:'DER', format:'UTF8'}
            ])
            params.format = params.format.toString('utf8')
            if(['DER', 'PEM'].includes(params.format.toUpperCase()))
                callback(null, this.privacyNode.getPublicKey(params.format.toUpperCase()))
            else
                callback('Invalid public key format in parameter (use "DER" or "PEM"):'+params.format, null)


        } else if(req.method=='admin_getTransactionProof') {
            params = RpcUtil.assertParams(req.params, [
                {name: 'hash', format:'Data32'}
            ])
            this.privacyNode.getProofOfTransaction(params.hash, (err, proof)=>{
                if(err) callback('Impossible to collect proof for hash '+params.hash+' because of '+err, null)
                else {
                    if(proof.input) {
                        delete proof.input.aes_key
                        delete proof.input.iv
                    }
                    callback(null, proof)
                }
            })
        } else callback(new RPCError('unknown method '+req.method, null, METHOD_NOT_FOUND), null);
    }

    close(cb) {
        // TODO close other resources but not the privacyNode that should close by the creator
        super.close(cb)
    }
}

module.exports = PrivacyRPCHandlerApp;