/**
 * Meant to implement https://github.com/ethereum/wiki/wiki/JSON-RPC
 * over the 2 simultaneous ethereum nodes
 * @Author guenoledc@yahoo.fr
 */

const async = require('async');
const EthUtils = require('ethereumjs-util');
const RpcUtil = require('ethereum-standalone-node/lib/rpc/rpc-util')
const makeRpc = RpcUtil.makeRpc
const RPCError = RpcUtil.RPCError;
const RPCResponse = RpcUtil.RPCResponse;
const methods = require('./rcp-methods')
const handleEncryption = require('../encryption').handleEncryption
const ParkedTransaction = require('./parked-transactions-handler').ParkedTransactions

module.exports = function handle_forwarding(req, callback) {
    let self=this // this is the privacyNode
    if(!req) throw new TypeError('Unexpected missing request in handle_forwarding')
    if(!req.method) throw new TypeError('Unexpected missing request.method in handle_forwarding')
    if(!req.params) req.params=[]
    
    function preProcessRequest(req) {
        // if private and/or privateFor are found in the object params[0] then move them in the last params
        // this is to enable existing web3 method to specify these info without calling the provider directly
        if(!req || !req.params || !Array.isArray(req.params) || req.params.length==0 || !req.params[0]) return req
        if(typeof req.params[0] !== 'object') return req
        let newParam = {}
        if( 'private' in req.params[0] ) {
            newParam.private = req.params[0].private
            delete req.params[0].private
        }
        if( 'privateFor' in req.params[0] ) {
            newParam.privateFor = req.params[0].privateFor
            delete req.params[0].privateFor
        }
        if(Object.keys(newParam).length>0) req.params.push(newParam)
        return req
    }
    function analysePreRequest(req) { // returns an object with decision
        // 1. Check if the caller has specified a target node, this always takes precedence
        let lastParam = req.params[req.params.length-1]
        if( lastParam && lastParam.private !== undefined ) { 
            target= lastParam.private?'private':'public'
            req.params.pop() // remove the last param
            return {target:target}
        }
        // analyse the cases of the request based on the method definition
        const method = methods[req.method]
        if( !method ) return {target:self.defaultNode}
        // 2. First check if this is a method that needs encryption otherwise other conditions will apply
        if( method.needEncryption && method.needEncryption(req) ) {
            let o = {target:'encrypt', for:method.for(req)}
            method.update(req)
            return o
        }
        // 3. Check case where the default node should be used
        if( method.useDefault ) return {target: self.defaultNode}
        // 4. Check case where the actions are to be performed on both and result merged
        if( method.merge ) return {target: 'both', handle:method.handle}
        // 5. Check case where the target is determined on existence of an address in private db
        if( method.useAddress ) return {target: 'useAddress', address:method.address(req), at:method.at(req)||undefined }
        // 6. Check case where the target is determined on existence of a block hash in the private db
        if( method.useWallet ) return {target: 'useWallet', address:method.address(req) }
        // 7. Check case where the target is determined on existence of a block hash in the private db
        if( method.useBlockHash ) return {target: 'useBlockHash', hash:method.hash(req) }
        // 8. Check case where the target is determined on existence of a transaction hash in the private db
        if( method.useTxHash ) return {target: 'useTxHash', hash:method.hash(req) }
        // 9. Check case where the target depends on the filter id origin
        if( method.useFilterId ) return {target: 'useFilterId', id:method.id(req) }

        // special case to handle metamask issue (https://github.com/MetaMask/metamask-extension/issues/3475)
        // if we reach here we have node target node indication
        if( req.method == 'eth_sendRawTransaction' ) return {target:'parkTransaction'}

        // default case, use default target node
        return {target: self.defaultNode }
    }
    function analysePostRequest(req) { // returns an object with decision
        // analyse the cases of the request based on the method definition
        const method = methods[req.method]
        if( !method ) return {action:'none'}

        // method has an onResponse field
        if( method.onResponse && typeof method.onResponse==='object' ) {
            const onResponse = method.onResponse
            if( onResponse.multiplexFilter ) return {action:'multiplexFilter'}
            if( onResponse.setLogOrigin ) return {action:'setLogOrigin'}
        }
        // default case, use default target node
        return {action:'none'}
    }
    req = preProcessRequest(req)
    let work = analysePreRequest(req)
    let postAction = analysePostRequest(req)
    async.doUntil( // iterate over the work situation until a real target has been identified or an error raised
        cb=>{
            switch (work.target) {
                case 'private':
                    this.forwardTo(true, req, (err, res) => {work.error = err; work.result=res; cb()})
                    break; 
                case 'public':
                    this.forwardTo(false, req, (err, res) => {work.error = err; work.result=res; cb()})
                    break;
                case 'both': 
                    this.forwardToBoth(req, (err, both)=>{
                        work.error = err; 
                        work.result=new RPCResponse(req, null, work.handle(both.private, both.public)); 
                        cb()
                    })
                    break
                case 'useWallet':
                    handle_useWallet(req, work, (err, newWork)=>{
                        if(err) work.error=new RPCError(err, null, RpcUtil.INVALID_PARAMS)
                        else work=newWork
                        cb()
                    })
                    break
                case 'useAddress':
                    handle_useAddress(req, work, (err, newWork)=>{
                        if(err) work.error=new RPCError(err, null, RpcUtil.INVALID_PARAMS)
                        else work=newWork
                        cb()
                    })
                    break
                case 'useTxHash':
                    handle_useTxHash(req, work, (err, newWork)=>{
                        if(err) work.error=new RPCError(err, null, RpcUtil.INVALID_PARAMS)
                        else work=newWork
                        cb()
                    })
                    break
                case 'useBlockHash':
                    handle_useBlockHash(req, work, (err, newWork)=>{
                        if(err) work.error=new RPCError(err, null, RpcUtil.INVALID_PARAMS)
                        else work=newWork
                        cb()
                    })
                    break
                case 'useFilterId':
                    handle_useFilterId(req, work, (err, newWork)=>{
                        if(err) work.error=new RPCError(err, null, RpcUtil.INVALID_PARAMS)
                        else work=newWork
                        cb()
                    })
                    break
                case 'encrypt': 
                    handleEncryption(self, req, work, (err, newWork)=>{
                        if(err) work.error=new RPCError(err, null, RpcUtil.INVALID_PARAMS)
                        else work=newWork
                        cb()
                    })
                    break
                case 'parkTransaction':
                    let res = ParkedTransaction.getFromPrivacyNode(self).parkRawTransaction(req)
                    work.result = {result:res}
                    cb()
                    break
                default:
                    work.error=new RPCError('method definition not implemented ['+work.target+']', null, RpcUtil.INTERNAL_ERROR);
                    cb()
                    break;
            }
        },
        ()=> (work.error!==undefined || work.result!==undefined), // until either an error or a result has been constructed
        err=>{
            if(err) work.error = err;
            if(work.error) return callback(work.error, null)
            if(work.result!==undefined) {
                handle_response(req, work, postAction, (err, newWork)=>{
                    if(err) work.error = err;
                    if(work.error) return callback(work.error, null)
                    if(work.result!==undefined) return callback(work.result.error, work.result.result)
                    return callback(new RPCError('Work object has no error or result after processing response!',null, RpcUtil.INTERNAL_ERROR))
                })
                return 
            }
            return callback(new RPCError('Work object has no error or result !',null, RpcUtil.INTERNAL_ERROR))
        }
    ) // end of the doUntil async function

    function handle_useWallet(request, work, cbNewWork) {
        self.forwardToBoth(makeRpc('eth_accounts'), (err,both)=>{
            if(both.private.result && both.private.result.includes(work.address)) return cbNewWork(null, {target:'private'})
            if(both.public.result && both.public.result.includes(work.address)) return cbNewWork(null, {target:'public'})
            cbNewWork('The provided wallet does not exists in either node')
        })
    }    
    function handle_useAddress(request, work, cbNewWork) {
        self.forwardTo(true, makeRpc('eth_addressInUse',[work.address, work.at]), (err,response)=>{
            //console.log('******handle_useAddress*******', work.address, response)
            if(response.error) console.warn('eth_addressInUse is not answering. Check this is a compatible node. '+response.error.message)
            if(response.result) return cbNewWork(null, {target:'private'})
            else return cbNewWork(null, {target:'public'})
        })
    }    
    function handle_useTxHash(request, work, cbNewWork) {
        // TODO: Handle some cache on the txHashes created or seen to avoid calling several time
        self.forwardToBoth(makeRpc('eth_getTransactionByHash', [work.hash]), (err,both)=>{
            //console.log('******handle_useTxHash*******', both)
            if(both.private.result && both.private.result.hash==work.hash) return cbNewWork(null, {target:'private'})
            if(both.public.result && both.public.result.hash==work.hash) return cbNewWork(null, {target:'public'})
            cbNewWork(null, {target:'both', result:new RPCResponse(request, null, null), error:null})
        })
    }    
    function handle_useBlockHash(request, work, cbNewWork) {
        // TODO: Handle some cache on the blockHashes seen to avoid calling several time
        self.forwardToBoth(makeRpc('eth_getBlockByHash', [work.hash, false]), (err,both)=>{
            //console.log('******handle_useBlockHash*******', both)
            if(both.private.result && both.private.result.hash==work.hash) return cbNewWork(null, {target:'private'})
            if(both.public.result && both.public.result.hash==work.hash) return cbNewWork(null, {target:'public'})
            cbNewWork(null, {target:'both', result:null, error:null})
        })
    }    


    function handle_useFilterId(request, work, cbNewWork) {
        let match = null
        let target
        if(work.id) match=work.id.match(/0x(?<target>01|02)(?<id>[0-9a-f]+)$/i)
        if(!match) return cbNewWork('invalid param at position 0 :'+request.params[0], null)
        if(match.groups.target=='01') target='private'
        if(match.groups.target=='02') target='public'
        // Attention, this is an assumption that all methods using useFilterId have the id on the first param
        request.params[0]='0x'+match.groups.id
        //console.log('******handle_useFilterId*******', target, request.params)
        return cbNewWork(null, {target:target})
    }    


    function handle_response(request, work, postAction, cbNewWork) {
        if(!postAction || !postAction.action ) return cbNewWork('Incorrect postAction structure. check code')
        if(work.result && work.result.error) return cbNewWork(work.result.error)
        
        if( postAction.action=='multiplexFilter') {// assumed that the response is a filter Id
            if(work.target=='private') work.result.result = '0x01'+EthUtils.stripHexPrefix(work.result.result)
            if(work.target=='public') work.result.result = '0x02'+EthUtils.stripHexPrefix(work.result.result)
            if(work.target=='both') work.result.result = 'NOT_IMPLEMENTED'            
        }
        if( postAction.action=='setLogOrigin') { // assume the response is an array
            work.result.result.forEach(item=>{
                if(!item.logIndex) return // this is not a log but a block or transaction hash -> ignore
                item.origin=work.target
            })

        }

        // default situation or when action == 'none'
        return cbNewWork(null, work)
    }
} // end of handle method
