/**
 * Meant to implement https://github.com/ethereum/wiki/wiki/JSON-RPC
 * over the 2 simultaneous ethereum nodes
 * @Author guenoledc@yahoo.fr
 */

const async = require('async');
const EthUtils = require('ethereumjs-util');
const RpcUtil = require('ethereum-standalone-node/lib/rpc/rpc-util')
const RPCError = RpcUtil.RPCError;



module.exports = function handle_web3_and_net(req, callback) {
    switch (req.method) {
        case 'web3_clientVersion':
            this.forwardToBoth(req, (err, both)=>{
                callback(err, 'private:'+both.private.result+', public:'+both.public.result)
            })
        break;
        
        case 'web3_sha3':
            if(!req.params || !req.params[0] || typeof req.params[0] !== 'string')
                callback(new RPCError('invalid parameter', null, RpcUtil.INVALID_PARAMS))
            else callback(null,EthUtils.bufferToHex(EthUtils.keccak256(req.params[0])));
        break;
        
        case 'net_version':
            this.forwardToBoth(req, (err, both)=>{
                callback(err, 'private:'+both.private.result+', public:'+both.public.result)
            })
        break

        case 'net_listening':
            this.forwardTo(false, req, callback)
        break
        
        default:
            callback(new RPCError('unknown method '+req.method, null, RpcUtil.METHOD_NOT_FOUND), null);
            break;
    }
}