pragma solidity ^0.5.1;

contract PrivateTxStorage {
    
  event newPrivateTransaction(bytes32 indexed id, bytes rsaYourKeyBase64);
  uint createdBlock;
  mapping(bytes32=>bytes[]) encryptedTransactions;
  mapping(bytes32=>uint256) encryptedTransactionBlocks;
  mapping(bytes32=>bool) encryptedTransactionClosed;
  mapping(bytes32=>address) encryptedTransactionSender;

  constructor() public {
    createdBlock = block.number;
  }
  function getStartingBlock() public view returns (uint) {
    return createdBlock;
  }

  function getEncryptedTransaction(bytes32 id, uint256 index) public view returns (uint256 count, bytes memory encryptedTx) {
    require(encryptedTransactionClosed[id]==true, "id not closed");
    require(encryptedTransactionSender[id]!=address(0x0), "id must exists");
    return (encryptedTransactionBlocks[id], encryptedTransactions[id][index]);
  }

  function setEncryptedTransaction(bytes32 id, bytes memory encryptedTx, bool close) public {
    require(encryptedTransactionClosed[id]==false, "id closed");
    require(encryptedTransactionSender[id]==address(0x0) || encryptedTransactionSender[id]==msg.sender, "id already used");
    encryptedTransactionSender[id] = msg.sender;
    encryptedTransactions[id].push(encryptedTx);
    encryptedTransactionBlocks[id] ++;
    encryptedTransactionClosed[id] = close;
  }

  function notify(bytes32 id, bytes memory rsaEncryptedKey) public {
    require(encryptedTransactionClosed[id]==true, "id not closed");
    require(encryptedTransactionSender[id]!=address(0x0), "id must exists");
    emit newPrivateTransaction(id, rsaEncryptedKey);
  }
}