const async = require('async')
const crypto = require('core-crypto-privacy')
const NodeRsa = require('node-rsa')
const Ledger = require('./ethereum-ledger')
const Wallet = require('ethereumjs-wallet')
const Transaction = require('ethereumjs-tx')
const EthUtils = require('ethereumjs-util');
const BN = EthUtils.BN

const RpcUtil = require('ethereum-standalone-node/lib/rpc/rpc-util')
const makeRpc = RpcUtil.makeRpc
const RPCError = RpcUtil.RPCError;
const RPCResponse = RpcUtil.RPCResponse;


function handleEncryption(self, request, work, cbNewWork) {
    if(!self) return cbNewWork('Expects a valid instance of the EthPrivacyHandler')
    if(!self.pki) return cbNewWork('Expects a valid pki instance in the EthPrivacyHandler')
    if(!self.getPrivateKey()) return cbNewWork('Expects the EthPrivacyHandler to have a private Key')
    if(!request) return cbNewWork('Expects a incoming json rpc request')
    if(!work || work.target!='encrypt' || !Array.isArray(work.for)) return cbNewWork('Expect a proper work object')

    let privKey = new NodeRsa(self.getPrivateKey())
    // get the participants publi keys from the PKI and ignore not found participants
    let pubKeys = work.for.map(name=>self.pki.getPublicKey(name)).filter(key=>!!key).map(key=>key.trim())
    // add the self public key always
    let pubKey = privKey.exportKey('pkcs1-public-pem').trim()
    if(!pubKeys.includes(pubKey)) pubKeys.push(pubKey)
    
    async.seq(
        cb=>{
            // if the transaction sent is not a raw transaction, convert it to a raw transaction with the private node
            if( ['eth_sendTransaction', 'personal_sendTransaction'].includes(request.method) ) {
               let req = makeRpc('personal_signTransaction', request.params)
               self.forwardTo(true, req, (err, response) => {
                   if(err) cb(err)
                   else if(response.error) cb(response.error.message, null)
                   else cb(null, response.result)
               })
           } else if(request.method == 'eth_sendRawTransaction')
               setImmediate( ()=> cb(null,request.params[0]) )
            else setImmediate( ()=>cb('Unexpected method in handle_encryption:'+request.method))
        },
        (rawTx, cb)=>{
           // prepare the raw transaction request 
           request = { 
                method:'eth_sendRawTransaction',
                params:[rawTx]
           }
           // TODO: play the transaction on the private node before encypting it to make sure it works ?
           // get the hash of the private transaction 
           let tx = new Transaction(rawTx)
           let hashToReturn= EthUtils.bufferToHex(tx.hash())
           setImmediate( ()=>cb(null, hashToReturn) )
        },
        (hashToReturn, cb)=>crypto.submitPrivateTransaction(request, self.getPrivateKey(), pubKeys, self.ledger, (err, result)=>{
            if(err) return cb(err)
            if(!hashToReturn) return cb('Unexpected situation where hashToReturn is null')
            // Here we should make the link between the hashes created on the public chain and the private transaction hash returned

            // Monitor the public transactions completion but do not wait for that to respond to the caller sendit it the list of txs
            self.ledger.waitTransactions(result.ledgerResult, 15*60*1000, (err, receipts)=>{
                if(err) console.error('Impossible to wait for transactions ', err)
                else {
                    Object.keys(receipts).forEach(hash=>
                        console.log('Receipt of ', 
                            hash, '=', receipts[hash].status==1?'Success':'Failed:'+receipts[hash].exception,
                            'gas =', receipts[hash].gasUsed
                            ))
                }
            })

            cb(null, hashToReturn)
        })
    )( (err, hashToReturn) => {
        if(err) cbNewWork(err)
        else cbNewWork(null, {target:'public', result:new RPCResponse(request, null, hashToReturn), error:null})
    })

}

// executed in the context of the node
function receiveATransactionToDecrypt(event) {
    // Convert to base 64 string
    event.rsaYourKeyBase64 = EthUtils.toBuffer(event.rsaYourKeyBase64).toString('base64')
    let self = this
    //console.log('receiveATransactionToDecrypt', event.rsaYourKeyBase64, event.blockNumber, event.transactionIndex)
    crypto.getPrivateTransaction(event.rsaYourKeyBase64, self.getPrivateKey(), self.ledger, (err, result)=>{
        //console.log('receiveATransactionToDecrypt', err, result)
        if(!err && !!result) { // there is no error and the result object is not null (ie decrypted)
            // check that the sender public key is known before accepting the message

            let sender = null
            if( new NodeRsa(self.getPrivateKey()).exportKey('pkcs1-public-der').toString('base64') == result.payload.senderPubKey)
                sender = 'self'
            else {
                let senderPubKey = new NodeRsa(Buffer.from(result.payload.senderPubKey, 'base64'),'pkcs1-public-der')
                sender = self.pki.whoIs(senderPubKey.exportKey('pkcs1-public-pem'))
            }
            if(!sender) return
            self.contextDataProvider.TEST_lastProofInput = result.input
            self.contextDataProvider.TEST_lastEvent = event
            // NOTE Ordering of the transactions should be respected somehow. The order is known by Tx(result.data).nonce
            //      but there are gaps in the sequence since not all privacy nodes receive all sender's transactions.
            //      somehow, the sender should inform its receiver of the gaps !
            //      For the prototype, we will assume that all transactions will be processed in order. (TODO: Review all 'NOTE Ordering')

            // get the public block of the event
            self.ledger.getBlock(event.blockNumber, (err, block)=>{
                if(err) console.error('Fail to get the public block '+event.blockNumber)
                else self.contextDataProvider.lastBlock = block
                
                // send the data as a request to the private node
                // Submit the request to the private blockchain/node 
                let req = makeRpc(result.data.method, result.data.params)
                console.log('Submit private transaction ', result.input.hash, req)
                self.forwardTo(true, req, (err, response)=>{
                    // Add the link of the private tx hash with the block & tx Index where the notification event was raised
                    if(!err && response.result) {
                        const linkInfo = {
                            id: result.input.hash,
                            aes_key: result.input.aes_key.toString('base64'),
                            iv: result.input.initVector.toString('base64'),
                            blockNumber:event.blockNumber, 
                            txIndex:event.transactionIndex,
                            sender: sender,
                        }
                        console.log('Link public id to private tx', result.input.hash, response.result)
                        self.forwardTo(true, makeRpc('db_putString', ['link',response.result, JSON.stringify(linkInfo)]), (err, response)=>{
                            if(err || !response.result) console.error('Fail saving the link between the private tx hash and the public crypto info the local db', err)
                        })  
                    }

                    event.blockNumber = EthUtils.bufferToHex(event.blockNumber)
                    console.log('SAVING THE LASTBLOCK','current='+self.ledger.lastBlock,  'new='+event.blockNumber)
                    
                    // Save in the private db the last time the public block has been handled
                    if(!err && new BN(EthUtils.toBuffer(self.ledger.lastBlock)).lte(new BN(EthUtils.toBuffer(event.blockNumber))) ) 
                                self.forwardTo(true, makeRpc('db_putHex', ['privacy','lastBlock', event.blockNumber]), (err, response)=>{
                                    if(!err) self.ledger.lastBlock = event.blockNumber
                                })                    
                })

            })

        } else if(err) console.error('Fail to handle received transaction:'+err)
    })
}

function initializeEventHandler(ethereumPrivacyNode, cb) {
    if(!ethereumPrivacyNode) throw new TypeError('Expects a valid instance of EthereumPrivacyNode')
    let self = ethereumPrivacyNode

    self.ledger.onEvent( receiveATransactionToDecrypt.bind(self) )
    setImmediate( ()=>cb() )
}

function loadHistoricalEvents(ethereumPrivacyNode, cb) {
    if(!ethereumPrivacyNode) throw new TypeError('Expects a valid instance of EthereumPrivacyNode')
    let self = ethereumPrivacyNode

    async.seq(
        // 1. get the latest block the events have been processed from
        cb=>self.forwardTo(true, makeRpc('db_getHex', ['privacy','lastBlock']), (err, response)=>{
            if(err) return cb(err)
            if(response.error) return cb(response.error.message)
            cb(null, response.result|| 0 )
        }),
        // 2. load events from last block and save last block on success
        (lastBlock, cb)=>{
            //console.log('Loading events from last block', lastBlock)
            self.ledger.lastBlock = '0x'+new BN(EthUtils.stripHexPrefix(lastBlock),'hex').addn(1).toString('hex')
            console.log('Load events from last block in the background', self.ledger.lastBlock)
            cb()
            self.ledger.loadEvents(self.ledger.lastBlock, null, event=>{
                if(event==null) // this is the end
                    return
                //console.log('Loading receiveATransactionToDecrypt', event.blockNumber, event.transactionIndex)
                receiveATransactionToDecrypt.call(self, event)
            } )
        }
    )(err=>{
        cb(err)
    })
    
}

function formatTransaction(tx) {
    // create a response respecting the format in https://github.com/ethereum/wiki/wiki/JSON-RPC#returns-28
    let transaction = {
        from: EthUtils.bufferToHex(tx.from),
        gas: EthUtils.bufferToHex(tx.gas),
        gasPrice: '0x'+new BN(tx.gasPrice).toString(16),
        hash: EthUtils.bufferToHex(tx.hash()),
        input: EthUtils.bufferToHex(tx.input),
        nonce: '0x'+new BN(tx.nonce).toString(16),
        to: '0x'+new BN(tx.to).toString(16),
        value: '0x'+new BN(tx.value).toString(16),
        v: '0x'+new BN(tx.v).toString(16),
        r: EthUtils.bufferToHex(tx.r),
        s: EthUtils.bufferToHex(tx.s)
    };

    return transaction;
}

function getProofOfTransaction(ethereumPrivacyNode, privateTxHash, onReady) {
    let _async = (typeof onReady === 'function')
    if(!_async) throw new TypeError('The implementation does not support asynchronous call')
    let self = ethereumPrivacyNode
    let proof = {
        ofTxHash: 'hash of the provate transaction for which the proof is created',
        input: {
            smAddress: 'eth address of the smartcontract in the public blockchain',
            id: 'id of the transaction stored in the public blockchain',
            aes_key: 'symetric key to decrypt the data in the public blockchain',
            iv: 'the init vector for the aes decypher algo',    
        },
        decryptedPayload:'the base64 encoded jwt in format header.payload.signature ',
        sender: {
            publicKey: 'public key of the sending privacy node of the tx, found in the decrypted jwt',
            nameAsOfNow: 'name of the sender in my pki or null if not found at time of the proof collection',
            nameAtTxTime: 'name of the sender public key as resolved by the kpi at time of the transaction',
        },
        request: 'decrypted initial request',
        storedAt: {
            txHash: 'public chain tx hash',
            blockHash: 'public chain block hash',
            timestamp: 'timestamp of the public block',    
            addressUsed: 'the eth address the node used to send the data in the public chain'
        },
        transactionFromRequest: {desc: 'a transaction object if the request.method is sendRawTransaction'},
        transactionReceipt: {desc: 'transaction receipt after injection in the private blockchain'}
    }


    async.seq(
        cb=>self.forwardTo(true, makeRpc('db_getString', ['link',privateTxHash]), cb),
        (linkInfo, cb)=>{
            linkInfo = linkInfo.result? JSON.parse(linkInfo.result) : null
            if(!linkInfo ) cb('This transaction does not have a proof stored')
            else cb(null, {linkInfo:linkInfo})
        },
        (info, cb)=>self.forwardTo(true, makeRpc('eth_getTransactionReceipt', [privateTxHash]), (err, receipt)=>{
            info.receipt = receipt ? receipt.result : null
            cb(err, info)
        }),
        (info, cb)=>crypto.getPrivateTransactionAES(
            info.linkInfo.id, 
            Buffer.from(info.linkInfo.aes_key, 'base64'), 
            Buffer.from(info.linkInfo.iv, 'base64'), 
            self.ledger,
            (err, decryptedInfo)=>{
                info.decryptedInfo = decryptedInfo
                cb(err, info)
        }),
        (info, cb)=>self.ledger.web3.eth.getBlock(+info.linkInfo.blockNumber, false, (err, block)=>{
            info.block = block
            cb(err, info)
        }),
        (info, cb)=>self.ledger.web3.eth.getTransactionFromBlock(+info.linkInfo.blockNumber, +info.linkInfo.txIndex, (err, transaction)=>{
            info.transaction = transaction
            cb(err, info)
        }),
        (info, cb)=>{ // Here we have all the info needed, let's construct the proof object
            
            proof.ofTxHash = privateTxHash

            proof.input.id = info.linkInfo.id
            proof.input.aes_key = info.linkInfo.aes_key
            proof.input.iv = info.linkInfo.iv
            proof.input.smAddress = self.ledger.smartContractInstance.address
            
            proof.decryptedPayload = info.decryptedInfo.jwToken
            proof.sender.publicKey=new NodeRsa( Buffer.from(info.decryptedInfo.payload.senderPubKey, 'base64'), 'pkcs1-public-der' ).exportKey('pkcs1-public-pem')
            proof.sender.nameAsOfNow=self.pki.whoIs(proof.sender.publicKey)
            proof.sender.nameAtTxTime=info.linkInfo.sender
            
            proof.request = info.decryptedInfo.data

            proof.storedAt.blockHash=info.transaction.blockHash
            proof.storedAt.txHash=info.transaction.hash
            proof.storedAt.timestamp=info.block.timestamp
            proof.storedAt.addressUsed=info.transaction.from

            if(proof.request && proof.request.method=='eth_sendRawTransaction' 
            && proof.request.params && proof.request.params.length==1) {
                let tx = new Transaction(proof.request.params[0])
                proof.transactionFromRequest = formatTransaction(tx)
            }

            proof.transactionReceipt = info.receipt

            cb()
        }
    )(err=>{
        onReady(err, proof)
    })

}

module.exports = {
    initializeEventHandler : initializeEventHandler,
    loadHistoricalEvents : loadHistoricalEvents,
    handleEncryption : handleEncryption,
    getProofOfTransaction : getProofOfTransaction
}