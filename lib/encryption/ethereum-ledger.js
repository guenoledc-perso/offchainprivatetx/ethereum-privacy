const _ = require('underscore');
const AbstractSharedLedger = require('core-crypto-privacy/ledger/abstract-shared-ledger');
const Web3 = require('web3');
require('ethereum-web3-plus');
const async = require('async')
const Transaction = require('ethereumjs-tx')
const EthUtils = require('ethereumjs-util')
const BN = EthUtils.BN
const Keystore = require('ethereum-standalone-node/lib/keystore')

// In Ethereum blockchain implementation using smart contract PrivateTxStorage

const smartContractName = 'PrivateTxStorage';
class EthereumLedger extends AbstractSharedLedger {
    constructor(options, web3Provider) {
        super();
        if(!web3Provider) throw new TypeError('Expects a valid web3 provider to create an EthereumLedger')
        if(!options || !options.address || !options.account) throw new TypeError('Expects options to be passed {account:"0x....", address:"0x...."}')
        
        this.web3 = new Web3( web3Provider );
        this.eth = this.web3.eth;
        // TODO: using a default account is not a final solution. it must use an anonymous servicing of account
        this.account = options.account ;
        this.passphrase = options.passphrase ;
        this.smartContractInstance = this.web3.instanceAt(smartContractName, options.address);
        
        // init a keystore
        if(!options.keystore) options.keystore='memory'
        if(options.keystore instanceof Keystore) this.keystore = options.keystore
        else this.keystore = new Keystore(options.keystore, 500)
        if(this.keystore.hasAccount(this.account))
            this.keystore.unlockAccount(this.account, this.passphrase, 0)
        else console.warn('EthereumLedger: The account '+this.account+' is not in the keystore')

        this.registeredEventCb = ()=>{}
        this.filter = null
        this.lastEventBlock = null
        this._loadingTransactions=false
    }
    _estimateGas(web3Method, params, maxGas, callback) {
        web3Method.estimateGas(...params, {from:this.account, to:this.smartContractInstance.address}, (e,gas)=>{
            if(e) console.error("ESTIMATE GAS Failed", e.message || e)
            gas = (gas && gas < maxGas ? gas : maxGas) //some node respond with a null gas or block.gasLimit if the transaction fails. Force to max 500000 because it should always be enough
            console.log('ESTIMATE GAS=',gas)
            callback(null, gas)
        })
    }
    _sendAsRawTransaction(web3Method, params, options, callback) {
        // for instance:
        // _sendAsRawTransaction(this.smartContractInstance.setEncryptedTransaction, txid, encrypted_tx, {gas: 4000000, nonce:'0x'+nonce.toString(16)}, (err, hash)=>cb(err, hash) )
        let self = this
        let chainId = '0x'+new BN(EthUtils.stripHexPrefix(self.web3.currentProvider.chainId)).toString('hex')
        let tx = {
            to: this.smartContractInstance.address,
            value:0, chainId: chainId, //gas:90000, 
            gasPrice: +self.web3.toWei(1, 'gwei')
        }
        if(web3Method=='new') { // Case for deploying contract
            delete tx.to
            Object.assign(tx, options)
            finish()
        } else {
            Object.assign(tx, options)
            tx.data = web3Method.getData(...params)
            finish()
        }
        function finish() {
            //console.log('PREPARING RAW TRANSACTION \n',self.account, JSON.stringify(tx))
            tx = new Transaction(tx)
            if(self.keystore.isLocked(self.account)) self.keystore.unlockAccount(self.account, self.passphrase, 500)
            tx = self.keystore.signTransaction(self.account, tx)
            //console.log('CHECKING from', self.account, tx.from.toString('hex'))
            tx = '0x' + tx.serialize().toString('hex')
            //console.log('RAW TRANSACTION TO PUBLIC NODE:', chainId, '\n', tx)
            self.web3.eth.sendRawTransaction(tx, callback)
        }
    }
    transactionRecorder(txid, encrypted_tx, participantsInfos, onCompletion) {
        txid = EthUtils.bufferToHex( EthUtils.toBuffer(EthUtils.addHexPrefix(txid)).slice(0,32) )
        let txs = [];
        let nonce 
        // break encrypted_tx into multiple buffers of 32x10 bytes
        let blockSize = 32*10 // TODO:see if we need to set a global setting or a constant here
        let initialBuffer = Buffer.from(encrypted_tx, 'base64')
        let buffers = []
        let nbBlock = Math.trunc( initialBuffer.length / blockSize)+1
        for(let i=0; i < nbBlock; i++)
            buffers.push(initialBuffer.slice(blockSize*i, blockSize*(i+1)))
        console.log('SENDING:', buffers.length, initialBuffer.length)
        async.seq(
            cb=>this.web3.eth.getTransactionCount(this.account, 'latest', (err, number)=>{nonce=new BN(number); cb(err)}),
            cb=>this._estimateGas(
                this.smartContractInstance.setEncryptedTransaction,
                [txid, EthUtils.bufferToHex(buffers[0]), false],
                500000,
                cb
                ),
            (gas,cb)=>async.eachOfSeries(
                buffers, 
                (buffer, index, cb)=>{
                    this._sendAsRawTransaction(
                        this.smartContractInstance.setEncryptedTransaction,
                        [txid, EthUtils.bufferToHex(buffer), index==buffers.length-1], 
                        {gas: gas, nonce:'0x'+nonce.toString(16)}, 
                        (err, hash)=>{
                            if(!err) { txs.push(hash); nonce.iaddn(1) }
                            cb(err)
                        })
                    },
                cb),
            cb=>async.eachSeries(participantsInfos, 
                (info,cb) => {
                    let infoHex = EthUtils.bufferToHex( Buffer.from(info, 'base64') )
                    this._sendAsRawTransaction(
                        this.smartContractInstance.notify,
                        [txid, infoHex], 
                        {gas: 80000, nonce:'0x'+nonce.toString(16)} , 
                        (err, hash)=>{
                            if(!err) { txs.push(hash); nonce.iaddn(1) }
                            cb(err)
                        })
                    }, 
                    cb )
        )(error=>{
            onCompletion(error, txs)
        })
    }

    getBlock(number, onBlock) {
        this.web3.eth.getBlock(number, false, onBlock)
    }

    waitTransactions(txs, timeout, cb) {
        //let _cb = cb
        //cb = (err, rec) =>{console.log('CB CALLED'); _cb(err, rec)}
        let receipts = {}
        txs.forEach(hash=>receipts[hash]=null)
        
        function checkTransactionReceipt(err, blockHash) {
            //console.log("NEW BLOCK WATCHED", blockHash)
            async.eachSeries( Object.keys(receipts).filter(hash=>receipts[hash]==null), // only not received hashes 
                (hash, cb)=>{
                self.web3.eth.getTransactionReceipt(hash, (err,receipt)=>{
                    //console.log("CHECK TRANSACTION COMPLETED:", hash, err, JSON.stringify(receipt))
                    if(!err) 
                        if(receipt)
                            receipts[hash] = receipt
                    cb(null)
                })
            }, err=>{
                if(err && bw) { bw.stopWatching(err=>{} ); if(timer) clearTimeout(timer); return cb(err, null) }
                // all tx received
                if( Object.keys(receipts).filter(hash=>receipts[hash]==null).length == 0 ) {
                    if(bw) bw.stopWatching(err=>cb(err, receipts))
                    if(timer) clearTimeout(timer); 
                    bw=null // to prevent another call
                }
            })
        }

        let timer = null
        let bw = this.web3.eth.filter('latest')
        let self = this
        bw.watch( checkTransactionReceipt )
        timer = setTimeout( ()=>{
            if(bw) bw.stopWatching(err=>{})
            bw=null
            timer=null
            cb('Timeout')
        }, timeout)
        // do a first check in case it has been mined before we activated the filter (it is the case with Ganache and geth --dev)
        checkTransactionReceipt(null, null)
    }


    transactionReader(txid, cb) {
        throw new Error('Sync transactionReader not implemented')
    }
    transactionReaderAsync(txid, cb) {
        txid = EthUtils.bufferToHex( EthUtils.toBuffer(EthUtils.addHexPrefix(txid)).slice(0,32) )
        let index = 0
        let buffers = []
        let self = this
        console.log('READING ENCRYPTED TX : ', txid)
        async.doWhilst(
            cb=>self.smartContractInstance.getEncryptedTransaction(txid, index++, (error, [count, encryptedTx]) => {
                //console.log('READING:', count.toNumber(), encryptedTx)
                buffers.push( EthUtils.toBuffer(encryptedTx) )
                cb(error, count.toNumber())
            }),
            nbBlocks=>index<nbBlocks,
            err=>{
                cb( err, Buffer.concat(buffers).toString('base64') )
            }
        )
    }
    _processLog(log, eventCb) {
        console.log("PROCESSING LEDGER LOG", log.blockNumber, log.transactionIndex)
        this.lastEventBlock = log.blockNumber
        eventCb({
            rsaYourKeyBase64: log.args.rsaYourKeyBase64, 
            blockNumber: log.blockNumber, 
            transactionIndex:log.transactionIndex
            })
    }
    // TODO: Replace with a proper event emmitter
    onEvent(eventCb) { 
        if(eventCb)
            this.registeredEventCb = eventCb 
        if(!this.filter) // the smart contract event registration is not done yet: do it
            this.startListeningFutureEvents('latest')
    } 
    loadEvents(from, to, eventCb) {
        console.log('ENTERING LOAD EVENTS')
        if(this._loadingTransactions) {
            setTimeout( this.loadEvents.bind(this, from, to, eventCb), 100)
            return
        }
        this._loadingTransactions=true
        const BNum = this.web3.BigNumber
        let self = this
        if(!from) from=0

        from = new BNum( from )
        
        async.seq(
            cb=>{
                console.log('CHECKING FROM BLOCK')
        
                self.smartContractInstance.getStartingBlock(cb)
            },
            (fromBlock, cb)=>{
                console.log('CHECKING TO BLOCK')
        
                if( from.lt(fromBlock) ) from=fromBlock;
                if(!to || to=='latest') self.eth.getBlockNumber( (e, num)=>cb(e, new BNum(num) ) )
                else cb(null, new BNum( to ) )
            },
            (toBlock, cb)=>{
                to = toBlock
                let filter = null
                async.whilst(
                    ()=>from.lte(to),
                    cb=>{
                        console.log('LOADING EVENTS BETWEEN', from.toString(), toBlock.toString() )
                        // use 2000 steps because infura does not support big range of blocks
                        toBlock = to.lt(from.add(2000)) ? to : from.add(2000)
                        filter=self.smartContractInstance.newPrivateTransaction( null, {
                                fromBlock: from, 
                                toBlock: toBlock         } )
                        filter.get( (err, logs)=> {
                            console.log('RECEIVING EVENTS', logs?logs.length:err)
                            if(err) return cb(err)
                            async.eachSeries(logs, 
                                (log, cb) => {
                                    self._processLog(log, event=>{eventCb(event); cb()})
                                }, 
                                err=>{ 
                                    from = from.add(2000)
                                    filter.stopWatching( ()=>cb(err) )
                                    filter = null
                                })
                        })
                    },
                    err=>{
                        // Listening future events
                        self.startListeningFutureEvents(to+1)
                        cb(err)
                    }
                )
            }
        ) (err=>{

            // indicate this is the end
            try{eventCb(null)}catch(failEndCall){}
            self._loadingTransactions=false
        })
    }
    startListeningFutureEvents(from) {
        let self = this
        if(self.filter) return
        this.filter = self.smartContractInstance.newPrivateTransaction( null, {
            fromBlock: from, 
            toBlock: 'latest'  }, (err, log)=>{
                if(err) {
                    console.log('Error in public ledger event listening', err)
                    // close and reopen the listener
                    self.close()
                    self.loadEvents(self.lastEventBlock, null)
                    return
                }
                self._processLog(log, self.registeredEventCb.bind(self) )
            } )
    }
    close(cb) {
        if(!cb) cb = err=>{}

        if(this._loadingTransactions) {
            setTimeout( this.close.bind(this, cb), 100)
            return
        }

        async.seq(
            cb=>{
                if(this.filter) this.filter.stopWatching(err=>{});
                this.filter=null;
                cb()
            },
            cb=>{
                if(this.keystore) this.keystore.stop(cb)
            }
        )(err=>{
            cb(err)
        })


    }

    static deploySmartContract(web3Provider, fromAccount, passphrase, keystore, onCompletion) {
        const options = {
            account:fromAccount,
            passphrase: passphrase,
            keystore:keystore,
            address:'unknown yet!'
        }
        let self = new EthereumLedger(options, web3Provider)
        
        let web3 = new Web3(web3Provider)
        let compiler = web3.solidityCompiler()
        let instance = compiler.getContract(smartContractName)
        if(instance) {
            let bytecode = compiler.getCode(smartContractName)
            web3.version.getNetwork( (err, version)=>{
                if(err) return onCompletion(err)
                web3Provider.chainId = version
                web3.eth.getTransactionCount(fromAccount, 'latest', 
                    (err, number)=>{
                    if(err) return onCompletion(err)
                    self._sendAsRawTransaction('new',[], {
                        data:bytecode, 
                        from:fromAccount, 
                        nonce:'0x'+new BN(number).toString(16), 
                        gas:685000 /** 643578 */, 
                        gasPrice:+web3.toWei(0.001,'gwei')},

                        (error, hash)=>{ 
                            if(error) return onCompletion(error)
                            self.waitTransactions([hash], 10*60*1000, (error, receipts)=>{
                                self.close()
                                if(error) return onCompletion(error)
                                let receipt = receipts[hash]
                                if(receipt.status == 0) return onCompletion('Deploy failed:'+receipt.exception)
                                if(receipt.contractAddress) onCompletion(null, receipt.contractAddress)
                                else onCompletion('The contract creation failed:'+receipt.exception)
                            })
                        })
                    })
            })
        } else onCompletion('Cannot find smart contract code '+smartContractName)
    }

}


module.exports = EthereumLedger;