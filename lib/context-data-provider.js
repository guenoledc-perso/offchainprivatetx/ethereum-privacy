const ContextDataProvider = require('ethereum-standalone-node/lib/context-data-provider')
const EthUtils = require('ethereumjs-util');
const BN = EthUtils.BN


class PrivacyContextDataProvider extends ContextDataProvider {
    constructor(privacyNode) {
        super()
        this.privacyNode = privacyNode
        this.lastBlock=null
    }

    setLastBlock(block) {
        if(!block) return
        this.lastBlock=block
    }
    getTimestamp() {
        if(this.lastBlock) return EthUtils.bufferToInt(this.lastBlock.timestamp)
        else return super.getTimestamp()
    }
}

module.exports = PrivacyContextDataProvider