[scheme]: ./documentation/Processing_Logic.png
# ethereum-privacy

Connects a public ethereum blockchain with an Ethereum Standalone node and use the public blockchain as a conduit for encrypted transaction transmission and persistence

## Logical scheme (Not accurate - to be redocumented)
A request received on the Privacy node must be either directed to the private node or the public node according to a specific business logic
- The Web3 provider will have a `defaultNode` parameter that can take value `'{private:true|false}'` and that will be added at the end of the json rpc params if not already present or if `{private:true, for:[...]}` is not already present
- In situation where the developper cannot control the web3 provider (Metamask, Remix, geth console) default rules will apply:
    - if the rpc method refers to a target address, the address is first looked up in the private db and if found the request is forwarded to the private node, otherwise to the public node
    - if the rpc method refers to a block request by number, the request is forwarded to the public node
    - if the rpc method refers to a block hash, the hash is search first in the private db and if found the request is forwarded to the private node, otherwise to the public node
    - if the rpc method refers to a transaction hash, the hash is search first in the private db and if found the request is forwarded to the private node, otherwise to the public node
    - based on the above, if a filter is created, the implementation will remember to what node the filter has been created and forward the eth_getFilterChanges to the correct node. Filter ids are modified compare the the one returned from the remote node to ensure multiplexing
    - If none of the rules above applies, the request is forwarded to the public node
    


![Processing logic][scheme]

## Demo server configuration
On Redhat (7.6 in Azure)
```sh
sudo yum update -y
sudo yum install -y git gcc-c++ make

curl -sL https://rpm.nodesource.com/setup_10.x | sudo -E bash -
sudo yum install -y nodejs

sudo npm install -g pm2 

sudo mkdir /data
sudo mkdir /data/ethereum-privacy
sudo useradd proto
sudo chown proto:proto /data/ethereum-privacy

sudo firewall-cmd --zone=public --add-port=8545/tcp --permanent
sudo firewall-cmd --reload

sudo curl -L -o /usr/bin/jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
sudo chmod +x /usr/bin/jq

sudo su - proto
cd /data/ethereum-privacy
git clone --depth 1 https://gitlab.com/guenoledc-perso/offchainprivatetx/pki-interface.git
git clone --depth 1 https://gitlab.com/guenoledc-perso/offchainprivatetx/core-crypto-privacy.git
git clone --depth 1 https://gitlab.com/guenoledc-perso/offchainprivatetx/ethereum-standalone-node.git
git clone --depth 1 https://gitlab.com/guenoledc-perso/offchainprivatetx/ethereum-privacy.git

mkdir ./datadir
mkdir ./datadir/keystore
cp ethereum-privacy/datadir/keystore/* ./datadir/keystore
mkdir ethereum-privacy/node_modules
ln -s ../../ethereum-standalone-node ethereum-privacy/node_modules/ethereum-standalone-node

pm2 install pm2-logrotate
pm2 set pm2-logrotate:max_size 5M
pm2 set pm2-logrotate:retain 10
pm2 set pm2-logrotate:rotateInterval '0 0 * * *'

ethereum-privacy/bin/git-pull-all.sh
pm2 start --cwd /data/ethereum-privacy/ethereum-privacy ethereum-privacy/bin/start-node-infura.sh

pm2 start --cwd /data/ethereum-privacy ethereum-privacy/bin/monitor-repo.sh --no-autorestart -c "*/5 * * * *"

wget https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
```

## Building the pki on each demo node
```sh
curl -s -X POST -d '{"jsonrpc":"2.0", "id":1, "method":"admin_getPublicKey", "params":["PEM"]}' https://qualif.docchain-api.ca-cib.com/privacy1 | jq -r .result > /tmp/pubkey1
curl -s -X POST -d '{"jsonrpc":"2.0", "id":1, "method":"admin_getPublicKey", "params":["PEM"]}' https://qualif.docchain-api.ca-cib.com/privacy2 | jq -r .result > /tmp/pubkey2
curl -s -X POST -d '{"jsonrpc":"2.0", "id":1, "method":"admin_getPublicKey", "params":["PEM"]}' https://qualif.docchain-api.ca-cib.com/privacy3 | jq -r .result > /tmp/pubkey3

node pki-interface/bin/deploy-pub-key.js --path datadir/pki --name 'node A' --pubkey /tmp/pubkey1
node pki-interface/bin/deploy-pub-key.js --path datadir/pki --name 'node B' --pubkey /tmp/pubkey2
node pki-interface/bin/deploy-pub-key.js --path datadir/pki --name 'node C' --pubkey /tmp/pubkey3
```