const util = require('./utils')
const chai = require('chai');  
const expect = chai.expect;    // Using Expect style
const async = require('async')

const EthereumPrivacyNode = require('../')

const rpcCall = util.rpcCall
const mkdirp = require('mkdirp')
const cleaner = require('ethereum-standalone-node/lib/util/store-cleaner')
const ESN = require('ethereum-standalone-node')
const PKINaive = require('pki-interface').Naive
const NodeRSA = require('node-rsa')
const initialize = require('./init-db-cases')
const waitForTx = initialize.waitTransactionReceipt
const SM = require('./ressources/SmartContract')
const sendEther = require('./ressources/createEtherSendTx')


describe('Create and initialize new Privacy Node instance', () => {
    let conf
    let node
    let provider
    let txHash
    before(function(done) {
        initialize(this, 'node',false, (err, config)=>{
            conf=config
            done(err)
        })
    });
    it('should create a node', function(done){
        this.timeout(3000)
        node = new EthereumPrivacyNode({
            publicNode: conf.publicNode,
            privateNode: {
                dbPath: __dirname+'/db/node/local', keystorePath:'memory', networkId:5000,
                trace: true
            },
            pki: {path: __dirname+'/db/node/mypki'},
            privateKey: new NodeRSA({b:1024}).exportKey('pkcs1-private-pem'),
            ledger: {
                keystore: 'memory',
                address: conf.publicLedgerAddress,
                account: 'unspecified',
                passphrase: null
            }
        })
        expect(node).to.have.property('publicNode')
        expect(node).to.have.property('pki')
        expect(node).to.have.property('getPrivateKey')
        expect(node).to.have.property('ledger')
        expect(node).to.have.property('_initialized', false)

        node.ledger.account=node.ledger.keystore.newAccount('password')
        node.ledger.keystore.unlockAccount(node.ledger.account, 'password', 0)
        done()
    });
    it('should initialize', (done) => {
        node.initialize(done)
    });

    it('should provide a web3 provider', ()=>{
        provider = node.web3Provider(true)
        expect(provider).not.to.be.null
        expect(provider).to.have.property('sendAsync')
    })

    it('should give ether to the ledger account', (done)=>{
        rpcCall(node.publicNode, 'eth_sendTransaction', 
            [{from:conf.publicCoinbase, to:node.ledger.account, value:1334417000000000}], 
            (hash)=>{
                waitForTx(node.publicNode, hash, 3000, (err, receipt)=>{
                    done()
                })
            })
    })

    it('should submit private transaction', (done)=>{
        let tx = sendEther(SM.newWallet, '0x5dfc8b442c73fe215244a57f9aab5bee1822351e', 0)
        let rawTx = '0x'+tx.serialize().toString('hex')
        rpcCall(provider, 'eth_sendRawTransaction', [rawTx, {privateFor:[]}], (hash)=>{
            expect(hash).match(/^0x[0-9a-fA-F]{64}$/)
            txHash = hash
            waitForTx(provider, hash, 3000, (err, receipt)=>{
                expect(err).to.be.null
                console.log(receipt)
                done()
            })
        })
    })

    it('should get the transaction proof', (done) => {
        node.getProofOfTransaction( txHash, (err, proof)=>{
            expect(err).to.be.null
            expect(proof).to.have.property('sender')
            expect(proof).to.have.property('input')
            //expect(proof).to.have.property('jwtSignature')
            expect(proof).to.have.property('storedAt')
            expect(proof).to.have.property('request')
            console.log(proof)
            done()
        } )
    });


    it('should fail getting transaction proof for unknown hash', (done) => {
        node.getProofOfTransaction( '0xe63537fcdcd0fe1f753b7a4172782ff119d437930aa0c61b6b16f2b970eede29', (err, proof)=>{
            expect(err).to.equal('This transaction does not have a proof stored')
            done()
        } )
    });


    it('should get the transaction proof via rpc call', (done) => {
        rpcCall(provider, 'admin_getTransactionProof', [txHash], (proof)=>{
            console.log(proof)
            done()
        })
    });

    after((done) => {
        node.close(err=>{
            if(conf.publicNode) conf.publicNode.close()
            if(conf.privateNode) conf.privateNode.close()
            provider.close(done)
        })
    });
});