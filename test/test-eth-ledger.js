const utils = require('./utils')
const EthereumLedger = require('../lib/encryption/ethereum-ledger')
const cleaner = require('ethereum-standalone-node/lib/util/store-cleaner')
const ESN = require('ethereum-standalone-node')
const Web3 = require('web3')

const chai = require('chai');  
const expect = chai.expect;    // Using Expect style
const async = require('async')

describe('Ethereum Ledger testing', () => {
    let ledger
    let events = []
    before(function(done) {
        this.timeout(5000)
        const path=__dirname+'/db/eth-ledger'
        cleaner.cleanLevelDbSync(path)
        const esn_public = new ESN({dbPath:path, keystorePath:'memory', networkId:1})
        let web3
        async.seq(
            cb=>esn_public.initialization(cb),
            cb=>{web3=new Web3(esn_public.web3Provider(false)); cb()},
            cb=>web3.personal.newAccount('password', (err, account)=>{web3.defaultAccount=account; cb(err)}),
            cb=>{
                web3.personal.unlockAccount(web3.defaultAccount, 'password', 0, (err, state)=>cb(err))
            },
            cb=>web3.currentProvider.sendAsync({jsonrpc:'2.0', id:1, method:'miner_setEtherbase', params:[web3.defaultAccount]}, (err, response)=>{
                cb(err)
            }),
            cb=>web3.eth.sendTransaction({from:web3.defaultAccount, to:'0x5dfc8b442c73fe215244a57f9aab5bee1822351e', value:0, gasPrice:0, gas:21000}, (err, hash)=>{
                setTimeout(cb, 200) // wait for the tx to be mined and ether be given to the coinbase
            }),
            cb=>EthereumLedger.deploySmartContract(web3.currentProvider, web3.defaultAccount, 'password' ,esn_public.keystore, (err, address)=>{
                expect(err).to.be.null
                if(!err) {
                    ledger = new EthereumLedger({
                        account:web3.defaultAccount,
                        address:address,
                        passphrase: 'password',
                        keystore: esn_public.keystore
                    }, web3.currentProvider)
                } 
                cb(err)
            }),
        )(err=>{
            done()
        })
    });
    it('should subscribe to events from the ledger', () => {
        ledger.onEvent( (event)=>{
            console.log('onEvent', event)
            events.push(event)
        } )
        
    });
    it('should send a transaction', (done) => {
        ledger.transactionRecorder('0x1234567', 'I4QjhHKYN0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0I4QjhHKYN0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0I4QjhHKYN0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0', 
            ['ASNFZ4kBI0VniQ==', 'mHZUMhCYdlQyEA=='], (err, hashes)=>{
            console.log('transactionRecorder:', err, '\n',hashes)
            expect(err).to.be.null
            expect(Array.isArray(hashes)).to.be.true
            //expect(hashes.length).to.equal(5)
            ledger.waitTransactions(hashes, 5000, (e, receipts)=>{
                expect(e).to.be.null
                expect(Object.keys(receipts).length).to.equal(hashes.length)
                done()
            })
        })
    });

    it('should have received events', () => {
        console.log('Events received', events)
        expect(events.length).to.equal(2)
    });

    it('should be able to read transaction content', (done) => {
        ledger.transactionReaderAsync('0x1234567', (err, result)=>{
            //console.log('transactionReaderAsync', err, result)
            expect(err).to.be.null
            expect(result).to.equal('I4QjhHKYN0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0I4QjhHKYN0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0I4QjhHKYN0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0N0knOUcpNHkjdDdJJzlHKTR5I3Q3SSc5Ryk0eSN0')
            done()
        })
    });

    it('should read no historical events', (done) => {
        ledger.loadEvents(0, 2, (event)=>{
            expect(event).to.be.null
            done()
        } )
    });
    it('should read historical events', (done) => {
        let count=0
        ledger.loadEvents(3, null, (event)=>{
            console.log('historical events', event)
            if(event) count++
            if(event==null) {
                expect(count==events.length)
                done()
            }
        } )
    });

    after(() => {
        if(ledger) ledger.close()
    });
});