const utils = require('./utils')
const chai = require('chai');  
const expect = chai.expect;    // Using Expect style
const RpcUtil = require('ethereum-standalone-node/lib/rpc/rpc-util')
const async = require('async')
const EthereumPrivacyNode = require('../index')

const rpcCall = utils.rpcCall
const initiateHandler = utils.initiateHandler
const initialize = require('./init-db-cases')

describe('Test the basic eth module', function() {
    let conf
    let provider

    before(function(done) {
        initialize(this, 'eth-basic', true, (err, config)=>{
            conf = config
            provider = conf.node.web3Provider(true)
            done()
        })
    });

    it('should call eth_protocolVersion on default node', (done) => {
        rpcCall(provider, 'eth_protocolVersion',[], response=>{
            expect(+response).to.equal(54)
            done()
        })
    });
    
    it('should call eth_syncing on default node', (done) => {
        rpcCall(provider, 'eth_syncing',[], response=>{
            expect(response).to.equal(false)
            done()
        })
    });
    
    it('should call eth_coinbase on each node', (done) => {
        let publicCoinbase
        rpcCall(provider, 'eth_coinbase',[{private:false}], response=>{
            expect(response).to.match(/^0x[0-9a-fA-F]{40}$/)
            publicCoinbase = response
            rpcCall(provider, 'eth_coinbase',[], response=>{
                expect(response).to.match(/^0x[0-9a-fA-F]{40}$/)
                expect(response).to.not.equal(publicCoinbase)
                done()
            })
        })
    });
    
    it('should call eth_mining on default node', (done) => {
        rpcCall(provider, 'eth_mining',[], response=>{
            expect(response).to.equal(true)
            done()
        })
    });
    
    it('should call miner_stop on default node', (done) => {
        rpcCall(provider, 'miner_stop',[], response=>{
            expect(response).to.equal(false)
            done()
        })
    });
    
    it('should create an account on each node and set it as coinbase', function(done) {
        this.timeout(3000) // this is only available when NOT using an arrow function
        async.parallel([
            cb=>rpcCall(provider, 'personal_newAccount', ['password', {private:true}], response=>{
                expect(response).match(/^0x[0-9a-fA-F]{40}$/)
                rpcCall(provider, 'miner_setEtherbase', [response, {private:true}], response=>{
                    cb()
                })
            }),
            cb=>rpcCall(provider, 'personal_newAccount', ['password', {private:false}], response=>{
                expect(response).match(/^0x[0-9a-fA-F]{40}$/)
                rpcCall(provider, 'miner_setEtherbase', [response, {private:false}], response=>{
                    cb()
                })
            }),
        ], err=>{
            done()
        })
        
    });
    let accounts=[]
    it('should get list of accounts from both nodes', (done) => {
        rpcCall(provider, 'eth_accounts', [{private:true}], response=>{
            accounts=response
            rpcCall(provider, 'eth_accounts', [{private:false}], response=>{
                accounts = accounts.concat(response)
                done()
            })
        })
    });

    it('should get the block number on each node', (done) => {
        async.parallel([
            cb=>rpcCall(provider, 'eth_blockNumber', [{private:true}], response=>{
                expect(response).to.equal('0x1')
                cb()
            }),
            cb=>rpcCall(provider, 'eth_blockNumber', [{private:false}], response=>{
                expect(response).to.equal('0x5')
                cb()
            }),
        ], err=>{
            done()
        })
    });

    it('should try to unlock an account that is in either node', function(done) {
        this.timeout(5000)
        async.each(accounts, (account, cb)=>{
            rpcCall(provider, 'personal_unlockAccount', [account, 'password', 0], response=>{
                expect(response).to.be.true
                cb()
            })
        }, done)
    })

    it('should sign a message on either account', function(done) {
        this.timeout(5000)
        async.each(accounts, (account, cb)=>{
            rpcCall(provider, 'eth_sign', [account, 'message to sign'], response=>{
                expect(response).match(/^0x[0-9a-fA-F]+$/)
                cb()
            })
        }, done)
    });

    after((done) => {
        conf.node.close(err=>{
            provider.close(done)
        })
        
    });
});