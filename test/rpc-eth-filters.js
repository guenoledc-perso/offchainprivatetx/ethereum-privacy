const utils = require('./utils')
const chai = require('chai');  
const expect = chai.expect;    // Using Expect style
const RpcUtil = require('ethereum-standalone-node/lib/rpc/rpc-util')
const async = require('async')

const SM = require('./ressources/SmartContract')
const rpcCall = utils.rpcCall
const initialize = require('./init-db-cases')

describe('Perform tests on filters', function() {
    let conf, provider
    
    before(function(done) {
        initialize(this, 'filters', true, (err, config)=>{
            conf = config
            provider = conf.node.web3Provider(true)
            done()
        })
    });
    it('shoud create a public filter on block', (done) => {
        rpcCall(provider, 'eth_newBlockFilter', [{private:false}], filterId=>{
            expect(filterId).match(/0x02[0-9A-F]+/)
            rpcCall(provider, 'eth_getFilterChanges', [filterId], response=>{
                rpcCall(provider, 'eth_uninstallFilter', [filterId], response=>{
                    done()
                })
            })
        })
    });

    it('shoud create a private filter on block', (done) => {
        rpcCall(provider, 'eth_newBlockFilter', [], filterId=>{
            expect(filterId).match(/0x01[0-9A-F]+/)
            rpcCall(provider, 'eth_getFilterChanges', [filterId], response=>{
                rpcCall(provider, 'eth_uninstallFilter', [filterId], response=>{
                    done()
                })
            })
        })
    });

    it('should create a log filter on smart contract', (done) => {
        let options = SM.getFilterRequestData(conf.smAddress)
        options.fromBlock = 0
        rpcCall(provider, 'eth_newFilter', [options, {private:false}], filterId=>{
            rpcCall(provider, 'eth_getFilterLogs', [filterId], response=>{
                expect(response.length).equal(1)
                expect(response[0]).to.have.property('origin', 'public')
                done()
            })
        })
    });
    it('should listen to a log filter on smart contract', (done) => {
        let options = SM.getFilterRequestData(conf.smAddress)
        options.fromBlock = 0
        rpcCall(provider, 'eth_newFilter', [options, {private:false}], filterId=>{
            setTimeout(
                ()=>rpcCall(provider, 'eth_getFilterChanges', [filterId], response=>{
                    expect(response.length).equal(1)
                    expect(response[0]).to.have.property('origin', 'public')
                    done()
                }),
                200 )
        })
    });
    it('should get logs on smart contract', (done) => {
        let options = SM.getFilterRequestData(conf.smAddress)
        options.fromBlock = 0
        rpcCall(provider, 'eth_getLogs', [options, {private:false}], response=>{
            expect(response.length).equal(1)
            expect(response[0]).to.have.property('origin', 'public')
            done()
        })
    });

    after((done) => {
        conf.node.close(err=>{
            provider.close(done)
        })
    });

});