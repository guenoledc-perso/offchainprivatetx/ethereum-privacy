const utils = require('./utils')
const chai = require('chai');  
const expect = chai.expect;    // Using Expect style
const RpcUtil = require('ethereum-standalone-node/lib/rpc/rpc-util')

const EthereumPrivacyNode = require('../index')

const rpcCall = utils.rpcCall
const initialize = require('./init-db-cases')

describe('Test the web3 and net modules', () => {
    let conf
    let provider

    before(function(done) {
        initialize(this, 'web3_net', true, (err, config)=>{
            conf=config
            provider = conf.node.web3Provider(true)
            
            done(err)
        })
    });
    it('should be able to call web3_clientVersion', (done) => {
        rpcCall(provider, 'web3_clientVersion',[], response=> {
            expect(response).to.equal('Ethereum-Standalone-Node/v0.0.1/6000')
            done()
        })
    });
    it('should calculate a sha3', (done) => {
        rpcCall(provider, 'web3_sha3', ['0x1234567889'], response => {
            expect(response).to.be.not.null
            expect(response).match(/^0x[0-9a-fA-F]{64}$/)
            done()
        })       
    });
    it('should call net_version on both nodes', (done) => {
        rpcCall(provider, 'net_version', [], response=>{
            expect(+response).to.eq(6000)
            rpcCall(provider, 'net_version', [{private:false}], response=>{
                expect(+response).to.eq(3)
                done()
            })
        })
    });
    it('should call net_listening on public node', (done) => {
        rpcCall(provider, 'net_listening',[], response=>{
            expect(response===false).to.be.true
            done()
        })
    });
    it('should change the default node', (done) => {
        rpcCall(provider, 'admin_getDefaultNode', [], currentDefaultNode=>{
            expect(currentDefaultNode).to.equal('private')
            rpcCall(provider, 'admin_setDefaultNode', ['public'], response=>{
                expect(response).to.equal('public')
                rpcCall(provider, 'eth_coinbase', [], response=>{
                    expect(response).match(/^0x[0-9a-fA-F]{40}$/)
                    rpcCall(provider, 'admin_setDefaultNode', [currentDefaultNode], response=>{
                        expect(response).to.equal(currentDefaultNode)
                        done()
                    })
                })
            })
        })
    });

    after((done) => {
        conf.node.close(err=>{
            provider.close(done)
        })
    });
});