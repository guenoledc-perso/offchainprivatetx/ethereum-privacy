// Need to understand what is going wrong with this. When using debugger the mocha elements are not in the global variable
if(global.describe === undefined) {
    const mocha = require('mocha')
    global.describe = mocha.describe
    global.it = mocha.it
    global.before = mocha.before
    global.after = mocha.after
} 
const chai = require('chai');  
const expect = chai.expect;    // Using Expect style

const mkdirp = require('mkdirp')
const cleaner = require('ethereum-standalone-node/lib/util/store-cleaner')
const ESN = require('ethereum-standalone-node')
//const PrivacyRpcHandlerApp = require('../lib/rpc/app-rpc-handler')
const EthereumPrivacyNode = require('../index')
const PKINaive = require('pki-interface').Naive
const NodeRSA = require('node-rsa')
//const Web3 = require('web3')

let reqId = 0
function rpcCall(provider, method, params, ready) {
    const req={ id: reqId++, jsonrpc: '2.0', method: method, params: params }
    provider.sendAsync(req, (error, response)=> {
        expect(error).to.be.null;
        expect(response).to.be.not.null;
        expect(response.error === undefined).to.equal(true, 'Fail on '+method+' with error '+JSON.stringify(response.error))
        expect(response).to.have.property('result')
        ready(response.result);
    })
}

function initiateHandler(basepath, done) {
    const path=__dirname+'/db/'+basepath
    mkdirp.sync(path)
    cleaner.cleanLevelDbSync(path+'/private')
    cleaner.cleanLevelDbSync(path+'/public')
    const esn_private = new ESN({dbPath:path+'/private', keystorePath:'memory', networkId:6000})
    const esn_public = new ESN({dbPath:path+'/public', keystorePath:'memory', networkId:3}) // simulating ropsten chainid
    let privateNode
    let publicNode
    const pki = new PKINaive(path+'/pki')
    const privKey = new NodeRSA({b:1024})
    
    //const publicProvider = new Web3.providers.HttpProvider('http://localhost:8545') // Geth
    esn_private.initialization(()=>{
        esn_public.initialization( ()=>{
            expect(esn_private).to.have.property('db').that.is.not.null;
            expect(esn_private).to.have.property('blockchain').that.is.not.null;
            expect(esn_private).to.have.property('vm').that.is.not.null;
            expect(esn_public).to.have.property('db').that.is.not.null;
            expect(esn_public).to.have.property('blockchain').that.is.not.null;
            expect(esn_public).to.have.property('vm').that.is.not.null;
            privateNode = esn_private.web3Provider(false)
            publicNode = esn_public.web3Provider(false)
            done(null, {
                privateNode: privateNode,
                publicNode: publicNode,
                pki: pki,
                privateKeystore: esn_private.keystore,
                privateKey: privKey.exportKey()
            })
        })
    })
}

module.exports = {
    rpcCall: rpcCall,
    initiateHandler: initiateHandler,
}