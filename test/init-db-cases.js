const utils = require('./utils')
const async = require('async')
const chai = require('chai');  
const expect = chai.expect;    // Using Expect style

const rpcCall = utils.rpcCall
const initiateHandler = utils.initiateHandler

const SM = require('./ressources/SmartContract')
const EthereumLedger = require('../lib/encryption/ethereum-ledger')

const EthereumPrivacyNode = require('../')


function waitTransactionReceipt(provider, hash, timeout, callback) {
    const start = Date.now()
    let receipt
    async.doWhilst(
        cb=>setTimeout( () => rpcCall(provider, 'eth_getTransactionReceipt', [hash], res=>{receipt=res; cb()}), 200),
        ()=> {
            return Date.now()-start<timeout && receipt==null
        },
        err=>callback(err || receipt?null:'timeout', receipt)
    )
}

module.exports = function initializeDbs(context, base, createNode, done) {
    let accounts=[]
    let publicCoinbase, privateCoinbase
    let publicNonce = 0, privateNonce = 0
    let config
    let ledger = null
    const targetAddress='0x77b82a312c3028abf16b273e4c317acc2b542237'
    context.timeout(10000)
        
    async.seq(
        // Create the 2 databases
        cb=>initiateHandler(base, (err, conf) => {
            config = conf
            cb()
        }),
        // create 2 accounts and set as coinbase and unlock
        cb=>async.parallel([
                cb=>rpcCall(config.privateNode, 'personal_importRawKey', ['0xceee921f6252f389fe940329b3c563fb1dbedf6bbd8cf31cb694274916b35bd6', 'password'], response=>{
                    expect(response).equals('0x5dfc8b442c73fe215244a57f9aab5bee1822351e') 
                    privateCoinbase=response
                    rpcCall(config.privateNode, 'miner_setEtherbase', [privateCoinbase], response=>{
                        rpcCall(config.privateNode, 'personal_unlockAccount', [privateCoinbase, 'password', 0], response=>{
                            expect(response).to.be.true
                            cb()
                        })
                    })
                }),
                cb=>rpcCall(config.publicNode, 'personal_importRawKey', ['0x22fe9c99f031430c0267ad3604b8de987c25dc19a945d56162d360f54f6d079d', 'password'], response=>{
                    expect(response).equals('0x7f69530cd5e74b828abb5c025da99be04a885bb7')
                    publicCoinbase=response
                    rpcCall(config.publicNode, 'miner_setEtherbase', [publicCoinbase], response=>{
                        rpcCall(config.publicNode, 'personal_unlockAccount', [publicCoinbase, 'password', 0], response=>{
                            expect(response).to.be.true
                            cb()
                        })
                    })
                }),
                // also import the same private key in the private keystore
                cb=>rpcCall(config.privateNode, 'personal_importRawKey', ['0x22fe9c99f031430c0267ad3604b8de987c25dc19a945d56162d360f54f6d079d', 'password'], response=>{
                    expect(response).equals('0x7f69530cd5e74b828abb5c025da99be04a885bb7')
                    cb()
                }),
            ], err=>{
                cb()
            }),

        // create public transactions (1st, to generate some ether on coinbase)
        cb=>rpcCall(config.publicNode, 'eth_sendTransaction', [{from:publicCoinbase, to:targetAddress,nonce:publicNonce++, value:0, gasPrice:0, gas:21000}], 
            response=>{
                waitTransactionReceipt(config.publicNode, response, 2000, (err, receipt)=>{
                    cb()
                })
            }),
        cb=>rpcCall(config.publicNode, 'eth_sendTransaction', [{from:publicCoinbase, to:targetAddress,nonce:publicNonce++, value:10000000, gasPrice:1000, gas:21000}], 
            response=>{
                waitTransactionReceipt(config.publicNode, response, 2000, (err, receipt)=>{
                    cb()
                })
            }),
        // Add smart contract 
        cb=>{
            let tx = SM.makeNewTx({gasPrice:1000}, publicCoinbase, publicNonce++ )
            rpcCall(config.publicNode, 'eth_sendTransaction', [tx], response=>{
                waitTransactionReceipt(config.publicNode, response, 2000, (err, receipt)=>{
                    expect(receipt.contractAddress).match(/^0x[0-9a-fA-F]{40}$/)
                    config.smAddress=receipt.contractAddress
                    cb()
                })
            })
        },
        cb=>{
            let tx=SM.makeSetTx(config.smAddress, 'Some text to set', {gasPrice:1000}, publicCoinbase, publicNonce++)
            rpcCall(config.publicNode, 'eth_sendTransaction', [tx], response=>{
                waitTransactionReceipt(config.publicNode, response, 2000, (err, receipt)=>{
                    expect(receipt.status).equal(1)
                    cb()
                })
            })
        },
        // create private transaction
        cb=>rpcCall(config.privateNode, 'eth_sendTransaction', [{from:privateCoinbase, to:targetAddress,nonce:privateNonce++, value:0, gasPrice:0, gas:21000}], 
            response=>{
                waitTransactionReceipt(config.privateNode, response, 2000, (err, receipt)=>{
                    cb()
                })
            }),
        // Deploy the Encryption smart contract
        cb=>{
            console.log('DEPLOYING ETHEREUM LEDGER SMART CONTRACT')
            config.publicNode.chainId='0x3' // TODO: WILL NOT WORK ON OTHER CHAIN THAN ROPSTEN
            EthereumLedger.deploySmartContract(config.publicNode, publicCoinbase, 'password', config.privateKeystore, (err, contract)=>{
                expect(err).to.be.null
                config.publicLedgerAddress = contract // set the ledger address without creating a new instance
                config.annonymousPublicAccount = publicCoinbase
                publicNonce++
                cb(err)
            })
        },
        // if requested create the node and add it in config
        cb=>{
            if( !createNode ) return cb()
            console.log('CREATING A PRIVACY NODE INSTANCE')
            node = new EthereumPrivacyNode({
                publicNode: config.publicNode,
                privateNode: config.privateNode,
                pki: config.pki,
                privateKey: config.privateKey,
                ledger: {
                    address: config.publicLedgerAddress,
                    account: publicCoinbase,
                    passphrase: 'password',
                    keystore: config.privateKeystore
                }
            })
            config.node = node
            node.initialize(cb)
        },
    )(err=>{
        config.privateCoinbase = privateCoinbase
        config.publicCoinbase = publicCoinbase
        config.privateNonce = privateNonce
        config.publicNonce = publicNonce     
        done(err,config)
    })
}
module.exports.waitTransactionReceipt = waitTransactionReceipt