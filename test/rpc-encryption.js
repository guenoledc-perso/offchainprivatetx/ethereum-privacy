const utils = require('./utils')
const chai = require('chai');  
const expect = chai.expect;    // Using Expect style
const RpcUtil = require('ethereum-standalone-node/lib/rpc/rpc-util')
const async = require('async')

const rpcCall = utils.rpcCall
const initialize = require('./init-db-cases')
const waitTransactionReceipt = initialize.waitTransactionReceipt
const SM = require('./ressources/SmartContract')
const NodeRsa = require('node-rsa')


describe('Perform test for privacy transaction', function() {
    let conf
    let provider
    let lastHash
    let smAddress
    before(function(done) {
        initialize(this, 'encryption', true, (err, config)=>{
            conf=config
            conf.part1Key = new NodeRsa({b:1024})
            conf.part2Key = new NodeRsa({b:1024})
            let mykey = new NodeRsa(conf.node.getPrivateKey())
            conf.node.pki.addPublicKey('part0', mykey.exportKey('pkcs1-public-pem'))
            conf.node.pki.addPublicKey('part1', conf.part1Key.exportKey('pkcs1-public-pem'))
            conf.node.pki.addPublicKey('part2', conf.part2Key.exportKey('pkcs1-public-pem'))
            provider = conf.node.web3Provider(true)
            done(err)
        })
    });

    it('should create a private smart contract via the encryption scheme', (done) => {
        let tx = SM.makeNewTx({chainId:6000}, SM.newWallet) // signed transaction
        tx = '0x'+tx.serialize().toString('hex')
        rpcCall(provider, 'eth_sendRawTransaction',[tx, {privateFor:['part0','part1','part2']}], hash=>{
            expect(hash).match(/^0x[0-9a-fA-F]{64}$/)
            lastHash = hash
            setTimeout( done , 10 )
        })
    });

    it('should retrieve the contract address created from the previously returned hash', function(done) {
        this.timeout(3000)
        waitTransactionReceipt(provider, lastHash, 3000, (err, receipt)=>{
            expect(err).to.be.null
            expect(receipt).to.have.property('contractAddress')
            expect(receipt.contractAddress).match(/^0x[0-9a-fA-F]{40}$/)
            //console.log("Receipt:", err, receipt)
            smAddress = receipt.contractAddress
            done()
        })
        
    });

    it('should make a change to a smart contract via the encryption scheme', function(done) {
        this.timeout(2500)
        let tx = SM.makeSetTx(smAddress, 'some text', {chainId:6000}, SM.newWallet) // signed transaction
        //tx = '0x'+tx.serialize().toString('hex')
        tx = {
            from:conf.privateCoinbase,
            to:smAddress,
            data: '0x'+tx.data.toString('hex'),
            value:'0x0',
            nonce: conf.privateNonce++,
            gasLimit: '0x'+tx.gasLimit.toString('hex'),
            gasPrice: 0
        }
        rpcCall(provider, 'eth_sendTransaction',[tx, {privateFor:['part1','part2']}], hash=>{
            expect(hash).match(/^0x[0-9a-fA-F]{64}$/)
            waitTransactionReceipt(provider, hash, 2000, (err, receipt)=>{
                expect(err).to.be.null
                expect(receipt).to.have.property('status', 1)
                done()
            })
        })
    });

    after((done) => {
        conf.node.close(err=>{
            provider.close(done)
        })
    });

});