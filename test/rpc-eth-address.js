const utils = require('./utils')
const chai = require('chai');  
const expect = chai.expect;    // Using Expect style
const RpcUtil = require('ethereum-standalone-node/lib/rpc/rpc-util')
const async = require('async')

const rpcCall = utils.rpcCall
const initialize = require('./init-db-cases')
const SM = require('./ressources/SmartContract')

describe('Perform test on addresses', function() {
    let conf
    let provider
    
    before(function(done) {
        initialize(this, 'address', true, (err, config)=>{
            conf=config
            provider = conf.node.web3Provider(true)
            
            done(err)
        })
    });

    it('should get the balance from the private node knowing that it is the default node', (done) => {
        rpcCall(provider, 'eth_getBalance',[conf.privateCoinbase, 'latest'], balance=>{
            expect(balance).match(/^0x[0-9a-fA-F]+$/)
            expect(+balance).gt(100000)
            done()
        })
    });
    it('should get the contract storage knowing that it is the default node', (done) => {
        rpcCall(provider, 'eth_getStorageAt',[conf.smAddress, '0x0', 'latest'], value=>{
            expect(value).match(/^0x[0-9a-fA-F]{64}$/)
            done()
        })
    });


    after((done) => {
        conf.node.close(err=>{
            provider.close(done)
        })
    });
});