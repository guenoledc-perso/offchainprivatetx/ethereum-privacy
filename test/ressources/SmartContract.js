const Web3 = require('web3');
const Wallet = require('ethereumjs-wallet');
const Account = require('ethereumjs-account').default; // attention, the lib returns an object with the class in 'default' prop
const Transaction = require('ethereumjs-tx');

// Create a not connected web3 instance to manipulate abi's
const web3 = new Web3();

// Wallets
let myWallet = Wallet.generate();
myWallet.lastNonce = 0;
//console.log('Wallet', myWallet.getAddressString(), myWallet.getPrivateKeyString(), myWallet.getPublicKeyString());




// Smart constract
/*
pragma solidity ^0.5.1;


contract small {
    event changedBy(string indexed, address);
    
    string private stored;   
    
    function set(string memory val) public {
        stored = val;
        emit changedBy(stored, msg.sender);
    }
    function get() view public returns (string memory val) {
        return stored;
    }
}

*/
const compileResult={
	"linkReferences": {},
	"object": "608060405234801561001057600080fd5b506103fd806100206000396000f3fe608060405260043610610046576000357c0100000000000000000000000000000000000000000000000000000000900480634ed3885e1461004b5780636d4ce63c14610113575b600080fd5b34801561005757600080fd5b506101116004803603602081101561006e57600080fd5b810190808035906020019064010000000081111561008b57600080fd5b82018360208201111561009d57600080fd5b803590602001918460018302840111640100000000831117156100bf57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f8201169050808301925050505050505091929192905050506101a3565b005b34801561011f57600080fd5b5061012861028a565b6040518080602001828103825283818151815260200191508051906020019080838360005b8381101561016857808201518184015260208101905061014d565b50505050905090810190601f1680156101955780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b80600090805190602001906101b992919061032c565b50600060405180828054600181600116156101000203166002900480156102175780601f106101f5576101008083540402835291820191610217565b820191906000526020600020905b815481529060010190602001808311610203575b505091505060405180910390207f70b8fe7843281b24ae760421a90c0172b3b1a3f98b684f38aa9fb0456472895b33604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390a250565b606060008054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156103225780601f106102f757610100808354040283529160200191610322565b820191906000526020600020905b81548152906001019060200180831161030557829003601f168201915b5050505050905090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061036d57805160ff191683800117855561039b565b8280016001018555821561039b579182015b8281111561039a57825182559160200191906001019061037f565b5b5090506103a891906103ac565b5090565b6103ce91905b808211156103ca5760008160009055506001016103b2565b5090565b9056fea165627a7a723058205df0fdfc9d628993f8f7712153b5a7fb3eabe60cc2e782c94eb9fa9241f27a660029",
	"opcodes": "PUSH1 0x80 PUSH1 0x40 MSTORE CALLVALUE DUP1 ISZERO PUSH2 0x10 JUMPI PUSH1 0x0 DUP1 REVERT JUMPDEST POP PUSH2 0x3FD DUP1 PUSH2 0x20 PUSH1 0x0 CODECOPY PUSH1 0x0 RETURN INVALID PUSH1 0x80 PUSH1 0x40 MSTORE PUSH1 0x4 CALLDATASIZE LT PUSH2 0x46 JUMPI PUSH1 0x0 CALLDATALOAD PUSH29 0x100000000000000000000000000000000000000000000000000000000 SWAP1 DIV DUP1 PUSH4 0x4ED3885E EQ PUSH2 0x4B JUMPI DUP1 PUSH4 0x6D4CE63C EQ PUSH2 0x113 JUMPI JUMPDEST PUSH1 0x0 DUP1 REVERT JUMPDEST CALLVALUE DUP1 ISZERO PUSH2 0x57 JUMPI PUSH1 0x0 DUP1 REVERT JUMPDEST POP PUSH2 0x111 PUSH1 0x4 DUP1 CALLDATASIZE SUB PUSH1 0x20 DUP2 LT ISZERO PUSH2 0x6E JUMPI PUSH1 0x0 DUP1 REVERT JUMPDEST DUP2 ADD SWAP1 DUP1 DUP1 CALLDATALOAD SWAP1 PUSH1 0x20 ADD SWAP1 PUSH5 0x100000000 DUP2 GT ISZERO PUSH2 0x8B JUMPI PUSH1 0x0 DUP1 REVERT JUMPDEST DUP3 ADD DUP4 PUSH1 0x20 DUP3 ADD GT ISZERO PUSH2 0x9D JUMPI PUSH1 0x0 DUP1 REVERT JUMPDEST DUP1 CALLDATALOAD SWAP1 PUSH1 0x20 ADD SWAP2 DUP5 PUSH1 0x1 DUP4 MUL DUP5 ADD GT PUSH5 0x100000000 DUP4 GT OR ISZERO PUSH2 0xBF JUMPI PUSH1 0x0 DUP1 REVERT JUMPDEST SWAP2 SWAP1 DUP1 DUP1 PUSH1 0x1F ADD PUSH1 0x20 DUP1 SWAP2 DIV MUL PUSH1 0x20 ADD PUSH1 0x40 MLOAD SWAP1 DUP2 ADD PUSH1 0x40 MSTORE DUP1 SWAP4 SWAP3 SWAP2 SWAP1 DUP2 DUP2 MSTORE PUSH1 0x20 ADD DUP4 DUP4 DUP1 DUP3 DUP5 CALLDATACOPY PUSH1 0x0 DUP2 DUP5 ADD MSTORE PUSH1 0x1F NOT PUSH1 0x1F DUP3 ADD AND SWAP1 POP DUP1 DUP4 ADD SWAP3 POP POP POP POP POP POP POP SWAP2 SWAP3 SWAP2 SWAP3 SWAP1 POP POP POP PUSH2 0x1A3 JUMP JUMPDEST STOP JUMPDEST CALLVALUE DUP1 ISZERO PUSH2 0x11F JUMPI PUSH1 0x0 DUP1 REVERT JUMPDEST POP PUSH2 0x128 PUSH2 0x28A JUMP JUMPDEST PUSH1 0x40 MLOAD DUP1 DUP1 PUSH1 0x20 ADD DUP3 DUP2 SUB DUP3 MSTORE DUP4 DUP2 DUP2 MLOAD DUP2 MSTORE PUSH1 0x20 ADD SWAP2 POP DUP1 MLOAD SWAP1 PUSH1 0x20 ADD SWAP1 DUP1 DUP4 DUP4 PUSH1 0x0 JUMPDEST DUP4 DUP2 LT ISZERO PUSH2 0x168 JUMPI DUP1 DUP3 ADD MLOAD DUP2 DUP5 ADD MSTORE PUSH1 0x20 DUP2 ADD SWAP1 POP PUSH2 0x14D JUMP JUMPDEST POP POP POP POP SWAP1 POP SWAP1 DUP2 ADD SWAP1 PUSH1 0x1F AND DUP1 ISZERO PUSH2 0x195 JUMPI DUP1 DUP3 SUB DUP1 MLOAD PUSH1 0x1 DUP4 PUSH1 0x20 SUB PUSH2 0x100 EXP SUB NOT AND DUP2 MSTORE PUSH1 0x20 ADD SWAP2 POP JUMPDEST POP SWAP3 POP POP POP PUSH1 0x40 MLOAD DUP1 SWAP2 SUB SWAP1 RETURN JUMPDEST DUP1 PUSH1 0x0 SWAP1 DUP1 MLOAD SWAP1 PUSH1 0x20 ADD SWAP1 PUSH2 0x1B9 SWAP3 SWAP2 SWAP1 PUSH2 0x32C JUMP JUMPDEST POP PUSH1 0x0 PUSH1 0x40 MLOAD DUP1 DUP3 DUP1 SLOAD PUSH1 0x1 DUP2 PUSH1 0x1 AND ISZERO PUSH2 0x100 MUL SUB AND PUSH1 0x2 SWAP1 DIV DUP1 ISZERO PUSH2 0x217 JUMPI DUP1 PUSH1 0x1F LT PUSH2 0x1F5 JUMPI PUSH2 0x100 DUP1 DUP4 SLOAD DIV MUL DUP4 MSTORE SWAP2 DUP3 ADD SWAP2 PUSH2 0x217 JUMP JUMPDEST DUP3 ADD SWAP2 SWAP1 PUSH1 0x0 MSTORE PUSH1 0x20 PUSH1 0x0 KECCAK256 SWAP1 JUMPDEST DUP2 SLOAD DUP2 MSTORE SWAP1 PUSH1 0x1 ADD SWAP1 PUSH1 0x20 ADD DUP1 DUP4 GT PUSH2 0x203 JUMPI JUMPDEST POP POP SWAP2 POP POP PUSH1 0x40 MLOAD DUP1 SWAP2 SUB SWAP1 KECCAK256 PUSH32 0x70B8FE7843281B24AE760421A90C0172B3B1A3F98B684F38AA9FB0456472895B CALLER PUSH1 0x40 MLOAD DUP1 DUP3 PUSH20 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF AND PUSH20 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF AND DUP2 MSTORE PUSH1 0x20 ADD SWAP2 POP POP PUSH1 0x40 MLOAD DUP1 SWAP2 SUB SWAP1 LOG2 POP JUMP JUMPDEST PUSH1 0x60 PUSH1 0x0 DUP1 SLOAD PUSH1 0x1 DUP2 PUSH1 0x1 AND ISZERO PUSH2 0x100 MUL SUB AND PUSH1 0x2 SWAP1 DIV DUP1 PUSH1 0x1F ADD PUSH1 0x20 DUP1 SWAP2 DIV MUL PUSH1 0x20 ADD PUSH1 0x40 MLOAD SWAP1 DUP2 ADD PUSH1 0x40 MSTORE DUP1 SWAP3 SWAP2 SWAP1 DUP2 DUP2 MSTORE PUSH1 0x20 ADD DUP3 DUP1 SLOAD PUSH1 0x1 DUP2 PUSH1 0x1 AND ISZERO PUSH2 0x100 MUL SUB AND PUSH1 0x2 SWAP1 DIV DUP1 ISZERO PUSH2 0x322 JUMPI DUP1 PUSH1 0x1F LT PUSH2 0x2F7 JUMPI PUSH2 0x100 DUP1 DUP4 SLOAD DIV MUL DUP4 MSTORE SWAP2 PUSH1 0x20 ADD SWAP2 PUSH2 0x322 JUMP JUMPDEST DUP3 ADD SWAP2 SWAP1 PUSH1 0x0 MSTORE PUSH1 0x20 PUSH1 0x0 KECCAK256 SWAP1 JUMPDEST DUP2 SLOAD DUP2 MSTORE SWAP1 PUSH1 0x1 ADD SWAP1 PUSH1 0x20 ADD DUP1 DUP4 GT PUSH2 0x305 JUMPI DUP3 SWAP1 SUB PUSH1 0x1F AND DUP3 ADD SWAP2 JUMPDEST POP POP POP POP POP SWAP1 POP SWAP1 JUMP JUMPDEST DUP3 DUP1 SLOAD PUSH1 0x1 DUP2 PUSH1 0x1 AND ISZERO PUSH2 0x100 MUL SUB AND PUSH1 0x2 SWAP1 DIV SWAP1 PUSH1 0x0 MSTORE PUSH1 0x20 PUSH1 0x0 KECCAK256 SWAP1 PUSH1 0x1F ADD PUSH1 0x20 SWAP1 DIV DUP2 ADD SWAP3 DUP3 PUSH1 0x1F LT PUSH2 0x36D JUMPI DUP1 MLOAD PUSH1 0xFF NOT AND DUP4 DUP1 ADD OR DUP6 SSTORE PUSH2 0x39B JUMP JUMPDEST DUP3 DUP1 ADD PUSH1 0x1 ADD DUP6 SSTORE DUP3 ISZERO PUSH2 0x39B JUMPI SWAP2 DUP3 ADD JUMPDEST DUP3 DUP2 GT ISZERO PUSH2 0x39A JUMPI DUP3 MLOAD DUP3 SSTORE SWAP2 PUSH1 0x20 ADD SWAP2 SWAP1 PUSH1 0x1 ADD SWAP1 PUSH2 0x37F JUMP JUMPDEST JUMPDEST POP SWAP1 POP PUSH2 0x3A8 SWAP2 SWAP1 PUSH2 0x3AC JUMP JUMPDEST POP SWAP1 JUMP JUMPDEST PUSH2 0x3CE SWAP2 SWAP1 JUMPDEST DUP1 DUP3 GT ISZERO PUSH2 0x3CA JUMPI PUSH1 0x0 DUP2 PUSH1 0x0 SWAP1 SSTORE POP PUSH1 0x1 ADD PUSH2 0x3B2 JUMP JUMPDEST POP SWAP1 JUMP JUMPDEST SWAP1 JUMP INVALID LOG1 PUSH6 0x627A7A723058 KECCAK256 0x5d CREATE REVERT 0xfc SWAP14 PUSH3 0x8993F8 0xf7 PUSH18 0x2153B5A7FB3EABE60CC2E782C94EB9FA9241 CALLCODE PUSH27 0x660029000000000000000000000000000000000000000000000000 ",
	"sourceMap": "26:311:0:-;;;;8:9:-1;5:2;;;30:1;27;20:12;5:2;26:311:0;;;;;;;"
}
const bytecode='0x'+compileResult.object
const abi = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "val",
				"type": "string"
			}
		],
		"name": "set",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "",
				"type": "string"
			},
			{
				"indexed": false,
				"name": "",
				"type": "address"
			}
		],
		"name": "changedBy",
		"type": "event"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "get",
		"outputs": [
			{
				"name": "val",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
];
const myContractFactory = web3.eth.contract(abi);

// Transaction creation

const default_opts = {
    gasPrice: '0x0',
    chainId: 6000
}

function makeNewTx(opts, wallet, nonce) {
    if(!opts) opts = default_opts;
    if(!wallet) wallet = myWallet;
    if(!nonce) nonce = wallet.lastNonce++;

    let contractCreationObj = {
        gasPrice: opts.gasPrice,
        gasLimit: 1000000, //opts.gasLimit,
        chainId : opts.chainId,
        nonce: nonce , // txcount of the sender's address. Must be consistent with what we put in the trie
        value: '0x0', // amount of wei to be transferred with the Tx
    //    to: '0x692a70d2e424a56d2c6c27aa97d1a86395877b3a', // target wallet or contract address, not used for contract creation
    //    data: '0x00' // data of the tx : empty for ether transfer, smart contract code + params for creation, method signature + params for smart contract call
        data: bytecode
    };

    let tx 
	if(typeof wallet.getPrivateKey === 'function') {
		tx = new Transaction(contractCreationObj);
		tx.sign(wallet.getPrivateKey());
	    //console.log('Tx Serialized', tx.serialize().toString('hex')); // this is the raw transaction, normally
	} else { // assume that the wallet is in fact an address
		tx = contractCreationObj
		delete tx.chainId
		tx.from = wallet
	}

//	console.log(JSON.stringify(tx));
    return tx;
}

function makeSetTx(contractAddress, value, opts, wallet, nonce) {
    if(!opts) opts = default_opts;
    if(!wallet) wallet = myWallet;
    if(!nonce) nonce = wallet.lastNonce++;

	let myContract = myContractFactory.at('0x0'); // a fake object to enable usage of code to data encoder 'getData'
	let setValueData = myContract.set.getData(value);
	//console.log("contract set value data: ", setValueData);
	let setValueTxObj = {
		gasPrice: opts.gasPrice,
		gasLimit: 100000, //constants.gasLimit,
		chainId : opts.chainId,
		nonce: nonce , // txcount of the sender's address. Must be consistent with what we put in the trie
		value: '0x0', // amount of wei to be transferred with the Tx
		to: contractAddress, // target wallet or contract address, not used for contract creation
		data: setValueData
	};
    let tx 
	if(typeof wallet.getPrivateKey === 'function') {
		tx = new Transaction(setValueTxObj);
		tx.sign(wallet.getPrivateKey());
	    //console.log('Tx Serialized', tx.serialize().toString('hex')); // this is the raw transaction, normally
	} else { // assume that the wallet is in fact an address
		tx = setValueTxObj
		delete tx.chainId
		tx.from = wallet
	}
	return tx
}

function makeGetTx(contractAddress, opts, wallet, nonce) {
    if(!opts) opts = default_opts;
    if(!wallet) wallet = myWallet;
    if(!nonce) nonce = wallet.lastNonce++;

	let myContract = myContractFactory.at('0x0'); // a fake object to enable usage of code to data encoder 'getData'
	let getValueData = myContract.get.getData();
	this.format = myContract.get.request().format; // to decode the result of the execution

	let getValueTxObj = {
		gasPrice: opts.gasPrice,
		gasLimit: 100000, //constants.gasLimit,
		chainId : opts.chainId,
		nonce: nonce , // txcount of the sender's address. Must be consistent with what we put in the trie
		value: '0x0', // amount of wei to be transferred with the Tx
		to: contractAddress, // target wallet or contract address, not used for contract creation
		data: getValueData
	};
    let tx 
	if(typeof wallet.getPrivateKey === 'function') {
		tx = new Transaction(getValueTxObj);
		tx.sign(wallet.getPrivateKey());
	    //console.log('Tx Serialized', tx.serialize().toString('hex')); // this is the raw transaction, normally
	} else { // assume that the wallet is in fact an address
		tx = getValueTxObj
		delete tx.chainId
		tx.from = wallet
	}
	return tx
}

function getFilterRequestData(contractAddress) {
	let myContract = myContractFactory.at(contractAddress); 
	let filter = myContract.changedBy()
	let data = {}
	return {address: filter.options.address, topics:filter.options.topics}

}

function decodeGetResult(data) {
    if(!data) return null;
    if(typeof data ==='string') data = new Buffer(data.slice(2), 'hex');
	let myContract = myContractFactory.at('0x0'); // a fake object to enable usage of code to data encoder 'getData'
	let format = myContract.get.request().format; // to decode the result of the execution
	return format('0x'+data.toString('hex'));
}

function decodeLogs(logs) {
	let myContract = myContractFactory.at('0x0'); // a fake object to enable usage of code to data encoder 'getData'
	let filter = myContract.allEvents();
	return logs.map( (log) => filter.formatter(log) );
}

module.exports = {
    newWallet : myWallet,
	makeNewTx: makeNewTx,
	makeSetTx: makeSetTx,
	makeGetTx: makeGetTx,
	getFilterRequestData: getFilterRequestData,
	decodeGetResult: decodeGetResult,
	decodeLogs: decodeLogs
}