const utils = require('./utils')
const chai = require('chai');  
const expect = chai.expect;    // Using Expect style
const RpcUtil = require('ethereum-standalone-node/lib/rpc/rpc-util')
const async = require('async')

const rpcCall = utils.rpcCall
const initialize = require('./init-db-cases')

describe('Perform test on blocks and transactions', function() {
    let conf, provider
    let publicBlockHash,privateBlockHash
    let publicTxHash,privateTxHash
    before(function(done) {
        initialize(this, 'block-tx', true, (err, config)=>{
            conf = config
            provider = conf.node.web3Provider(true)
            done()
        })
    });
    it('should get the latest public block with transactions', (done) => {
        rpcCall(provider, 'eth_getBlockByNumber', ['latest', true, {private:false}], response=>{
            publicBlockHash = response.hash
            publicTxHash = response.transactions[0].hash
            done()
        })
    });
    it('should get the latest private block with transactions', (done) => {
        rpcCall(provider, 'eth_getBlockByNumber', ['latest', true, {private:true}], response=>{
            privateBlockHash = response.hash
            privateTxHash = response.transactions[0].hash
            done()
        })
    });

    it('should get the public block by hash without knowing the node', (done) => {
        rpcCall(provider, 'eth_getBlockByHash', [publicBlockHash, false], response=>{
            expect(response).not.to.be.null
            expect(response.hash).to.equal(publicBlockHash)
            done()
        })
    });

    it('should get the private block by hash without knowing the node', (done) => {
        rpcCall(provider, 'eth_getBlockByHash', [privateBlockHash, false], response=>{
            expect(response).not.to.be.null
            expect(response.hash).to.equal(privateBlockHash)
            done()
        })
    });

    it('should get the private transaction by hash / index without knowing the node', (done) => {
        rpcCall(provider, 'eth_getTransactionByBlockHashAndIndex', [privateBlockHash, 0], response=>{
            expect(response).not.to.be.null
            expect(response.hash).to.equal(privateTxHash)
            done()
        })
    });

    it('should get the public transaction receipt without knowing the node', (done) => {
        rpcCall(provider, 'eth_getTransactionReceipt', [publicTxHash], response=>{
            expect(response).not.to.be.null
            expect(response.transactionHash).to.equal(publicTxHash)
            done()
        })
    });

    after((done) => {
        conf.node.close(err=>{
            provider.close(done)
        })
    });

});