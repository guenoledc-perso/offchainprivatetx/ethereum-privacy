DEV=$1 # either DEV or PROD (or else)

gitrepo="https://gitlab.com/guenoledc-perso/offchainprivatetx/"
folders_deps="core-crypto-privacy pki-interface ethereum-standalone-node"
for folder in $folders_deps
do
	echo Installing package $gitrepo$folder.git
	git clone --depth 1 $gitrepo$folder.git
	cd $folder
	npm ci
	cd ..
done

folder_main="ethereum-privacy"
echo Installing package $gitrepo$folder_main.git

if [ $DEV != "DEV" ]
then
	git clone --depth 1 $gitrepo$folder_main.git
	mkdir $folder_main/node_modules
fi

#for folder in $folders_deps
#do
#	ln -s ../../$folder $folder_main/node_modules/$folder
#done
cd $folder_main

npm ci

cd ..