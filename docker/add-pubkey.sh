
# Expect 2 parameters
nodeurl=$1
nodename=$2
echo ADDING PUBLIC KEY FOR $nodename from $nodeurl

curl -s -X POST -d '{"jsonrpc":"2.0", "id":1, "method":"admin_getPublicKey", "params":["PEM"]}' $nodeurl |jq -r .result > /tmp/pubkey_"$nodename".pub

cd /privacy
node ethereum-privacy/deploy-pub-key.js --path /data/pki --name "$nodename" --pubkey /tmp/pubkey_"$nodename".pub