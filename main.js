const async = require('async')
const fs = require('fs')
const Web3 = require('web3')
//const WebSocketProvider = require('web3-providers-ws')
const WebSocketProvider = require('./lib/rpc/websocket-provider')
const EthereumPrivacyNode = require('./index')
const Keystore = require('ethereum-standalone-node/lib/keystore')
/** interprets params (same as in geth)
Node configuration
  --datadir "/Users/guenole/Library/Ethereum"  Data directory for the databases and keystore
  --keystore value                             Directory for the keystore (default = inside the datadir)
Public node definition
  --publicnode.protocol http|ipc
  --publicnode.url http://localhost:8545 | <filepath to ipc socket>
  --publicnode.user <user for connection>
  --publicnode.password <password>
  --publicnode.timeout <http waiting in ms>
  
RPC Service
  --rpc                  Enable the HTTP-RPC server
  --rpcaddr value        HTTP-RPC server listening interface (default: "localhost")
  --rpcport value        HTTP-RPC server listening port (default: 8545)
  --rpcapi value         API's offered over the HTTP-RPC interface
  --rpccorsdomain value  Comma separated list of domains from which to accept cross origin requests (browser enforced)
  --ws                   Enable the WS-RPC server
  --wsaddr value         WS-RPC server listening interface (default: "localhost")
  --wsport value         WS-RPC server listening port (default: 8546)
  --wsapi value          API's offered over the WS-RPC interface
  --wsorigins value      Origins from which to accept websockets requests
*/

const commandLineArgs = require('command-line-args')
const commandLineUsage = require('command-line-usage')

let optionDefinitions = [
  { name: 'config', type: String, description: 'a json file where the config can be loaded. The same parameters as in the command line can be applied. Command line parameters always take precedence if specified'},
  { name: 'datadir', type: String, defaultValue:'datadir', description: 'Data directory for the databases and keystore (default: ./datadir)'},
  { name: 'keystore', type: String, defaultValue: '{datadir}/keystore', description:'Directory for the keystore (default: <datadir>/keystore)' },
  { name: 'networkid', alias: 'i', type: Number, defaultValue:6000, description:'Network identifier (integer, could use official numbers, default 6000)'},
  { name: 'rpcport', type: Number, defaultValue: 8546, description:'HTTP-RPC server listening port (default: 8546)'},
  { name: 'rpcaddr', type: String, defaultValue: 'localhost', description:'HTTP-RPC server listening interface (default: localhost)'},
  { name: 'rpcapi', type: String, defaultValue: 'eth,net,personal', description:'API\'s offered over the HTTP-RPC interface (not implemented yet - all apis are exposed)' },
  { name: 'rpctrace', type: Boolean, defaultValue: 'false', description:'Activate the console trace of the json-rpc request/response' },
  { name: 'privatekey', type:String, description:'the path to the private key (PKCS1 PEM format) of the node (default: <datadir>/nodekey). If innexistent, a new one is created'},

  { name: 'publicnode.protocol', type:String, defaultValue:'http', description:'protocol to reach the public ethereum node (http|ipc|ws, default:http)'},
  { name: 'publicnode.url', type:String, defaultValue:'http://localhost:8545', description:'ipc/http/ws url to connect to the public node (default: http://localhost:8545)'},
  { name: 'publicnode.user', type:String, description:'http user credential. No default'},
  { name: 'publicnode.password', type:String, description:'http password credential. No default'},
  { name: 'publicnode.timeout', type:Number, description:'http/ws request timeout. No default'},
  { name: 'publicnode.deploy', type:Boolean, defaultValue:false, description:'deploy the privacy smart contract and display the address. Will stop after deployment'},
  { name: 'publicnode.address', type:String, description:'the address of the privacy smart contract, takes precedence on publicnode.deploy'},
  { name: 'publicnode.account', type:String, description:'the address of the account to use to address the public node. If missing it will use the first available account in the node'},
  { name: 'publicnode.passphrase', type:String, description:'the passphrase to unlock the account. If ommitted the account is expected to be unlocked'},
  { name: 'publicnode.trace', type:Boolean, defaultValue:false, description:'Activate the json-rpc console tracing with the public node'},

  { name: 'privatenode.unlockaccounts', type:Boolean, defaultValue:false, description:'specify if local accounts should be unlocked by default'},
  { name: 'privatenode.passphrase', type:String, defaultValue:'password', description:'specify the passphrase to be used to unlock all local accounts'},
  { name: 'privatenode.trace', type:Boolean, defaultValue:'false', description:'Activate the json-rpc console trace to the private node'},
  {
    name: 'help',
    alias: 'h',
    type: Boolean,
    description: 'Display this usage guide.'
  }
]
let options = commandLineArgs(optionDefinitions) 

// handle a config file in complement of the options passed
if(options.config) {
  if(fs.existsSync(options.config))
  try {
    let config = JSON.parse(fs.readFileSync(options.config))
    // now set the default values in the optionDefinitions
    function setDefault(config, prefix) {
      if(!prefix) prefix=''
      Object.keys(config).forEach( optkey=>{
        if(typeof config[optkey]==='object') setDefault(config[optkey], optkey+'.')
        else {
          let def = optionDefinitions.find(def=>def.name==prefix+optkey)
          if(def) def.defaultValue=config[optkey]
        }
      })
    }
    setDefault(config)
    // seconf parsing of options with the new defaults
    options = commandLineArgs(optionDefinitions) 
  } catch (failReadingConfigFile) {
    console.error('Cannot read the config file '+options.config, failReadingConfigFile)
    process.exit(1)
  }
} 
options.keystore = options.keystore.replace('{datadir}', options.datadir)

if (options.help) {
  const usage = commandLineUsage([
    {
      header: 'Ethereum Privacy Node',
      content: 'A nodejs implementation of a Ethereum Privacy Node that handle private transactions via public Ethereum blockchain.'
    },
    {
      header: 'Options',
      optionList: optionDefinitions
    },
    {
      content: 'Project home: {underline https://gitlab.com/guenoledc-perso/offchainprivatetx}'
    }
  ])
  console.log(usage)
  process.exit(1)
} else {
  //console.log(options)
}

// transform options with dot (".") into object for easier manipulation
Object.keys(options).forEach(key=>{
  parts = key.split('.')
  function buildPart(obj, parts, value) {
    if(parts.length==1) obj[parts[0]]=value
    else {
      if(!obj[parts[0]] ) obj[parts[0]]={}
      buildPart(obj[parts[0]], parts.slice(1), value)
      delete options[key]
    }
  }
  buildPart(options, parts, options[key])
})



// INSTALL PROCESS HANDLING
process.on('uncaughtException', (err) => {
    console.log('Caught exception:', err);
});

let publicProvider
let node
let provider


function gracefullStop(cb) {
    if(!cb) cb=()=>{}
    // do some stuff on the servers
    if(provider) provider.close()
    if(node) 
      node.close(err=>{
        if(err) console.error('FAILURE CLOSING NODE RESOURCES', err)
        cb(err)
        process.exit(0)
      })
    else {
      cb()
      process.exit(0)
    }
   
}
process.on('SIGTERM', ()=>{
    console.log('Stopping process gracefully requested via SIGTERM')
    gracefullStop()
})
process.on('SIGINT', ()=>{
    console.log('Stopping process gracefully requested via SIGINT')    
    gracefullStop()
})


//console.log('OPTION DEF:\n', JSON.stringify(optionDefinitions, null, 2))
if(options.rpctrace || options.publicnode.trace || options.privatenode.trace)
  console.log('OPTIONS ARE:\n', JSON.stringify(options,null,2))

// Handle public node actions
if(options.publicnode.protocol=='http') publicProvider=new Web3.providers.HttpProvider(options.publicnode.url, options.publicnode.timeout, options.publicnode.user, options.publicnode.password)
if(options.publicnode.protocol=='ipc') publicProvider=new Web3.providers.IpcProvider(options.publicnode.url, require('net'))
if(options.publicnode.protocol=='ws') publicProvider=new WebSocketProvider(options.publicnode.url, {timeout:options.publicnode.timeout, trace:options.publicnode.trace})

if(!publicProvider) {
  console.error('Specify either a http or ipc protocol to contact the public node')
  gracefullStop()
}
let web3 = new Web3(publicProvider)

/* dont't need to wait for the public node is available.
if(!web3.isConnected())  {
  console.error('the targetted public node is not available')
  gracefullStop()
}
*/

/////////////////////////////////////////////////////////////
// THIS IS THE STARTING POINT OF THE PROCGRAM
/////////////////////////////////////////////////////////////
let tasksToRun 
if(options.publicnode.deploy) tasksToRun=[initLedger, gracefullStop]
else tasksToRun=[createNode, startServer]
async.series( tasksToRun, err=>{
  if(err) {
    console.error('Failed to start the process', err)
    gracefullStop()
  } 
})



function initLedger(cb) {
  async.seq(
    cb=>{
      if( !options.publicnode.address || options.publicnode.deploy ) {
        const EthereumLedger = require('./lib/encryption/ethereum-ledger')
        EthereumLedger.deploySmartContract(publicProvider, options.publicnode.account, options.publicnode.passphrase, new Keystore(options.keystore,0), (err, address)=>{
          if(err) cb(err)
          else {
            console.log('=====================================================================================')
            console.log('| SMART CONTRACT CREATED AT: [ '+address+' ]')
            console.log('=====================================================================================')
            console.log('| Now run node with --publicnode.address '+address)
            console.log('=====================================================================================')
            cb(null, address)
          }
        })
      } else cb(null, options.publicnode.address)
    },
    (address, cb)=>{
      if(!address) return cb('Impossible to find a privacy ledger address')
      options.publicnode.address = address
      cb()
    }
  )(cb)
  
}

function createNode(cb) {
  const NodeRSA = require('node-rsa')
  const mkdirp = require('mkdirp');
  const fs = require('fs')

  try {
    mkdirp.sync(options.datadir+'/data', 0777)
    mkdirp.sync(options.keystore, 0777)
    if(!options.privatekey) { 
      options.privatekey=options.datadir+'/nodekey'
      if( !fs.existsSync(options.privatekey) ) { // generate a key and save it to datadir
        let key = new NodeRSA({b:1024})
        fs.writeFileSync(options.privatekey, key.exportKey('pkcs1-private-pem'), 'utf8')
      }
    }
    
    node = new EthereumPrivacyNode({
      privateNode:{dbPath:options.datadir+'/data', keystorePath:options.keystore, networkId:options.networkid, trace:false},
      publicNode: publicProvider,
      pki: {path: options.datadir+'/pki'},
      privateKey: {path: options.privatekey},
      ledger: {
          address: options.publicnode.address,
          account: options.publicnode.account,
          passphrase: options.publicnode.passphrase,
          keystore: options.keystore
      }
    });
  } catch (failCreatingNode) {
    return cb(failCreatingNode)
  }
  node.initialize( err=>{
    if(err) return cb(err)
    provider = node.web3Provider(options.rpctrace); // true to trace on console the requests
    if(options.privatenode.unlockaccounts)
      setImmediate(unlockAccounts)
    cb()
  })
}

function unlockAccounts() {
  provider.sendAsync({jsonrpc:"2.0", id:0, method:'eth_accounts', params:[{private:true}]}, (err, response)=>{
    if(response && Array.isArray(response.result))
      async.each(response.result, (account, cb)=>{
        provider.sendAsync({jsonrpc:"2.0", id:0, method:'personal_unlockAccount', params:[account, options.privatenode.passphrase, 0, {private:true}]}, (err, response)=>{
          if(err) console.error('Could not unlock account '+account+' because ', err)
          cb()
        })
      }, (err)=>{

      })
  })
}

function startServer(cb) {
  HttpHandler(provider, options)
  cb()
}



function HttpHandler(provider, options) {
  let http=require('http')
  console.log('STARTING HTTP SERVER')
  function addAccessControlHeaders(request, response) {
    if(!request.headers.origin) return // no origin, test also here if CORS is allowed
    response.setHeader('Access-Control-Allow-Origin', request.headers.origin);
    response.setHeader('Vary', 'Origin');
  }
  function onOptions(request, response) {
    addAccessControlHeaders(request, response)
    response.writeHead(200, {
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'POST',
      'Access-Control-Max-Age': 600,
      'Vary': 'Access-Control-Request-Method',
      'Vary': 'Access-Control-Request-Headers',
      "Content-Type" : "text/plain; charset=utf-8"
    })
    response.end('')
  }
  let server=http.createServer( 
    function(request,response) {
      //console.log('request headers', request.headers)
      //console.log('request.method', request.method)
      if(request.method=='OPTIONS') return onOptions(request, response)
      if(request.method=='GET') { 
        console.log('GET ', request.url)
        response.writeHead(200, {"Content-Type" : "application/json"}); response.end('{status:"ok"}\n');
        return
      }
      if(request.method!='POST') { response.writeHead(404, {}); response.end(''); return}
      // TODO : Fail if wrong method, wrong content-type, wrong data size
      let data = new Buffer(0)
      request.on('data', chunk => {
        //console.log(data, chunk)
        data = Buffer.concat([data,chunk])
      });
      request.on('end', () => {
        //console.log('Request is ', data.toString())
        provider.sendAsync(data.toString('utf8'), (error, jsonResponse)=> {
          addAccessControlHeaders(request, response)
          response.writeHead(error?500:200, {"Content-Type" : "application/json"});
          response.end(jsonResponse+'\n');  
        })
      });
  });
  server.on('listening', ()=>console.log('HTTP-RPC service started on http://'+options.rpcaddr+':'+options.rpcport))
  server.on('error', (error)=>{
    if(error.code=='EADDRINUSE') {
      console.error(error.message, '\nRetrying')
      setTimeout(() => {
        server.close();
        server.listen(options.rpcport, options.rpcaddr);
      }, 1000);
    } else console.error('HTTP server error', error)
  })
  server.listen(options.rpcport, options.rpcaddr);
}