FROM node:11.13.0-alpine

RUN apk add curl jq

RUN apk add --no-cache --virtual .build-deps \
    gcc g++ make \
    git \
    python

COPY docker /tmp 

RUN mkdir /data && mkdir /privacy && mkdir /privacy/ethereum-privacy
COPY .ethereum_contracts /privacy/ethereum-privacy/.ethereum_contracts
COPY lib /privacy/ethereum-privacy/lib
COPY *.js /privacy/ethereum-privacy/
COPY package*.json /privacy/ethereum-privacy/

RUN cd /privacy/ethereum-privacy && npm ci


# Cleaning installed build dependencies
RUN apk del .build-deps 


WORKDIR /privacy/ethereum-privacy
ENTRYPOINT [ "node","main.js"]