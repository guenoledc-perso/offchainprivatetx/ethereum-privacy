#localaddr=`ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'`
localaddr=localhost
echo Starting node on address $localaddr

public_trace=--publicnode.trace
#public_trace=

node main.js --publicnode.protocol ws $public_trace --publicnode.url wss://goerli.infura.io/ws/v3/9cd9be4913f24139b8f8f8a968ed7de4 --publicnode.address 0xb2f89ce619a9a6bf35d72d7a74e1996f10915fc4 --rpcport 8545 --rpcaddr $localaddr --publicnode.account 0x7a020b20e76eeb2af986fe525c822ac3557ba1ed --publicnode.passphrase password --privatekey datadir/nodekey --privatenode.unlockaccounts --privatenode.passphrase password
