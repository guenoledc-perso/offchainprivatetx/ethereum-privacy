
need_reload=false
folders="core-crypto-privacy pki-interface ethereum-standalone-node ethereum-privacy"
for folder in $folders
do
	cd $folder
	git remote update # To get the remote refs
	local=`git rev-parse master`
	remote=`git rev-parse origin/master`
	echo "Compare hash on folder $folder: $local <=> $remote"
	if [ "$local" != "$remote" ]
	then
		need_reload=true
	fi
	cd ..

done

if [ $need_reload == true ]
then
	echo "Need to reload"
	ethereum-privacy/bin/git-pull-all.sh
else
	echo "Nothing to change"
fi