localaddr=127.0.0.1
echo Starting node on address $localaddr

node main.js --rpctrace --datadir /tmp/datadir --publicnode.protocol ws --publicnode.url wss://ropsten.infura.io/ws/v3/9cd9be4913f24139b8f8f8a968ed7de4 --publicnode.address 0xd5d36550a1d7971b4fa68be066ed4ef4f85a46e1 --rpcport 8545 --rpcaddr $localaddr --publicnode.account 0x7a020b20e76eeb2af986fe525c822ac3557ba1ed --publicnode.passphrase password --privatenode.unlockaccounts --privatenode.passphrase password
