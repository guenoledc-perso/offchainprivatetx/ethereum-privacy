
folders="core-crypto-privacy pki-interface ethereum-standalone-node ethereum-privacy"
for folder in $folders
do
	echo Installing package $folder
	cd $folder
	git checkout .
	git pull
	npm ci
	if [ "$folder" == "ethereum-privacy" ]
	then
		npm install ../core-crypto-privacy 
		npm install ../pki-interface
		npm install ../ethereum-standalone-node 
	fi
	cd ..
done

pm2 restart start-node-infura