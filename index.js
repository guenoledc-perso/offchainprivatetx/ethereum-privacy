const fs = require('fs')
const async = require('async')
const ESN = require('ethereum-standalone-node')
const PKI = require('pki-interface')
const AbstractLedger = require('core-crypto-privacy/ledger/abstract-shared-ledger')
const EthereumLedger = require('./lib/encryption/ethereum-ledger')
const Encryption = require('./lib/encryption')
const Web3Provider = require('ethereum-standalone-node/lib/rpc/web3-provider')
const RPCHandler = require('./lib/rpc/app-rpc-handler')
const PrivacyContextDataProvider = require('./lib/context-data-provider')
const rsa = require('node-rsa');


function implements(obj, funcName) {
    return obj && obj[funcName] && (obj[funcName] instanceof Function);
}
    

function isWeb3Provider(obj) {
    return implements(obj, 'sendAsync') && implements(obj, 'send') 
}
function isClosable(obj) {
    return implements(obj, 'close')
}

function makeGetPrivateKeyFunct(data) {
    
    if(typeof data === 'string') return ()=>data
    if(data && data.path) return ()=>fs.readFileSync(data.path, 'utf8')
    if(typeof data === 'function') return data

    throw new TypeError('Expects a PEM private key, a {path:<path to private key file>} or a ()=>privateKey function')
}


class EthereumPrivacyNode {
    constructor(options) {
        const self=this
        this._initTasks = [] // array of cb=>{ do something ; cb()}
        this._cleaningTasks = [] // array of cb=>{ do something ; cb()}
        // public node
        if( isWeb3Provider(options.publicNode) ) this.publicNode = options.publicNode
        else throw new TypeError('Expects a valid web3 provider for the public Node')

        this.contextDataProvider = new PrivacyContextDataProvider(this)

        // private node
        if( isWeb3Provider(options.privateNode) ) this.privateNode = options.privateNode
        else {
            // NOTE Ordering: set the private node as accepting nonce gaps
            options.privateNode.inPrivacyNode=true
            options.privateNode.context = this.contextDataProvider
            // create the private node
            const esn = new ESN(options.privateNode)
            this._initTasks.push((cb)=>{
                esn.initialization(()=>{
                    self.privateNode = esn.web3Provider(options.privateNode.trace || false)
                    cb()
                })
            })
            this._cleaningTasks.push(cb=>{
                esn.graceFullStop(cb)
            })
            
        }

        // set the defualt node thac can be changed via the rpc handler
        this.defaultNode = 'private'

        // PKI
        if( options.pki instanceof PKI.Abstract ) this.pki = options.pki
        else if(options.pki && options.pki.path) this.pki = new PKI.Naive(options.pki.path)
        else throw new TypeError('Expects a valid implementation of pki interface')

        // Private key
        this.getPrivateKey = makeGetPrivateKeyFunct(options.privateKey)

        // Ledger or address of the smart contract that implements the ledger
        if( options.ledger instanceof AbstractLedger ) this.ledger = options.ledger
        else if(options.ledger && options.ledger.address ) this.ledger = new EthereumLedger(options.ledger, this.publicNode)
        else throw new TypeError('Expects either an AbstractLedger instance or option to initialize EthereumLedger')
        this._initTasks.push(cb=>Encryption.initializeEventHandler(self, cb))
        this._initTasks.push(cb=>Encryption.loadHistoricalEvents(self, cb))

        // Loader of versions (ie ChainId) in each node provider
        this._initTasks.push(cb=>{
            self.forwardToBoth({jsonrpc:'2.0', id:1, method:'net_version'}, (err, both)=>{
                if(err) return cb(err)
                console.log('VERSIONS', both)
                self.privateNode.chainId = both.private.result
                self.publicNode.chainId = both.public.result
                cb()
            })
        })

        this._initialized = false
    }
    initialize(cb) {
        const tasks = this._initTasks
        this._initTasks = [] // empty to prevent double initialization
        const self = this
        async.series( tasks, err=>{
            if(!err) self._initialized = true
            else self._initTasks = tasks // put back in case of error for potential retry
            cb(err)
        } )
    }
    close(cb) {
        if(!cb) cb = err=>{}
        if(!this._initialized) return cb()
        // release resources properly
        let tasks = []
        if( isClosable(this.ledger) ) tasks.push( this.ledger.close.bind(this.ledger) )
        if( isClosable(this.publicNode) ) tasks.push( this.publicNode.close.bind(this.publicNode) )
        if( isClosable(this.privateNode) ) tasks.push( this.privateNode.close.bind(this.privateNode) )
        if( isClosable(this.pki)) tasks.push( this.pki.close.bind(this.pki) )
        tasks = tasks.concat(this._cleaningTasks)
        this._cleaningTasks = [] // empty to prevent double execution
        const self = this
        async.series( tasks, err=>{
            if(!err) this._initialized=false
            cb(err)
        })
    }

    forwardTo(isPrivate, request, callback) { // response is {json response}
        //console.log('====>', isPrivate?'Private':'Public', request)
        if(isPrivate) this.privateNode.sendAsync(request, callback)
        else this.publicNode.sendAsync(request, callback)
    }
    forwardToBoth(request, callback) { // response is {private:{json response}, public:{json response}}
        let self=this
        async.parallel([
            async.reflect(cb=>self.forwardTo(true, request, cb)),
            async.reflect(cb=>self.forwardTo(false, request, cb)),
        ], (err, both)=>{
            // err must always be null
            // both.length must be 2
            let result = {}
            if(both[0].error) both[0].value=new RPCResponse(request, both[0].error,null).toFormat()
            if(both[1].error) both[1].value=new RPCResponse(request, both[1].error,null).toFormat()
            result.private=both[0].value
            result.public=both[1].value
            callback(null, result)
        })
    }

    web3Provider(trace) {
        return new Web3Provider( new RPCHandler(this, trace) )
    }

    getPublicKey(format) {
        const formatting = {
            'DER': (key)=> key.exportKey('pkcs1-public-der').toString('base64'),
            'PEM': (key)=> key.exportKey('pkcs1-public-pem')
        }
        if(!format in formatting) format = 'DER'
        let key = new rsa(this.getPrivateKey())
        return formatting[format](key)
    }

    getProofOfTransaction(txHash, onReady) {
        if(!onReady) onReady = (_err, _result)=>{}
        //if(!this.contextDataProvider.TEST_lastProofInput) return onReady('noInput')
        //const input = this.contextDataProvider.TEST_lastProofInput
        Encryption.getProofOfTransaction(this, txHash, onReady)
    }
}

module.exports = EthereumPrivacyNode